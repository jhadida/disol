
CC = clang++
# CC = g++
DRAYN_SRC= ../jcl/src

WARNING = -Wall
DEFINE = -DDRAYN_DEBUG_MODE -DDISOL_INTERRUPT_N=0
INCLUDE = -I"$(DRAYN_SRC)"
CXXFLAGS = $(DEFINE) $(INCLUDE) $(WARNING) -std=c++11

clean:
	rm -f bin/*.o 

disol:
	$(CC) -c -o bin/disol.o $(CXXFLAGS) src/disol.cpp
