
[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)

# Differential Solvers library

DiSol is a C++ library built on top of [Drayn](https://gitlab.com/jhadida/drayn) to solve systems of differential equations.
The design resembles that of Boost's [`odeint`](http://headmyshoulder.github.io/odeint-v2/) library, but with added constrains on the state-type and memory allocation, and using a pub-sub pattern to provide hooks during integration.
There are several solvers currently implemented; explicit and implicit, with fixed or adaptive time-steps. Suggestions for improvement or future developments are most welcome (please open [an issue](https://gitlab.com/jhadida/disol/issues)).
