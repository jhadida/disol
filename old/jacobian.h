
//==================================================
// @title        jacobian.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



DISOL_NS_START

template <class S>
struct stepper_jacobian: public stepper<S,2,true>
{
    using parent         = stepper<S,2,false>;
    using system_type    = S;
    using time_type      = typename parent::time_type;
    using value_type     = typename parent::value_type;
    using property_type  = typename parent::property_type;
    using handle_type    = typename parent::handle_type;

    // ----------  =====  ----------

    /**
     * If I is the identity, and / is mrdivide:
     *
     * next.x = cur.x + ( cur.dxdt / cur.jac )*( expm(cur.dt*cur.jac) - I )
     */

    double step( handle_type& d, property_type& prop )
    {
        DRAYN_DASSERT_RVAL( d.valid(), 0.0, "Invalid handle.");

        const uidx_t nd = d.cur.size();
        system_type& sys = *d.sys;

        // Set current dxdt and jac
        sys.derivative ( d.cur.t, d.cur.x, d.cur.dxdt );
        sys.jacobian   ( d.cur.t, d.cur.x, d.cur.dfdx, d.cur.dfdt );

        // Export to Armadillo
        auto Jtran = dr::dr2arma_mat( d.cur.dfdx.transpose(), true, false ); // deep copy of the transpose
        auto f     = dr::dr2arma_col( static_cast<dr::array<value_type>>(d.cur.dxdt), false, false );

        // dxdt / J = J' \ f = solve( J', f )
        f = arma::solve( Jtran, f );

        // dt * J
        d.cur.dfdx *= d.cur.dt;
        arma::mat J_dt   = dr::dr2arma_mat( d.cur.dfdx, false, false );
        arma::mat expJdt = arma::expmat( J_dt );

        // Set final state
        d.next.t = d.cur.t + d.cur.dt;
        for ( uidx_t c = 0; c < nd; ++c )
        {
            d.next.x[c] = d.cur.x[c];
            for ( uidx_t r = 0; r < nd; ++r )
                d.next.x[c] += f(r) * ( expJdt(r,c) - (r==c) );
        }

        return 0.0;
    }
};

DISOL_NS_END
