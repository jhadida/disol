
//==================================================
// @title        import.cpp
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



DISOL_NS_START

bool mx_extract_problem( const dr::mx_struct& q, InitialValueProblem<double>& ivp )
{
    ASSERT_RF( q, "[set.ivp] Invalid input." );
    ASSERT_RF( q.has_fields({"t_start","t_end","x_start"}), "[set.ivp] Missing field(s)." );

    ASSERT_RF( dr::mx_extract_scalar(q["t_start"],ivp.t_start), "[set.ivp] Could not extract 't_start'." );
    ASSERT_RF( dr::mx_extract_scalar(q["t_end"],ivp.t_end), "[set.ivp] Could not extract 't_end'." );
    ASSERT_RF( dr::mx_extract_vector(q["x_start"],ivp.x_start), "[set.ivp] Could not extract 'x_start'." );

    if ( q.has_field("b_size") ) {
        ASSERT_RF( dr::mx_extract_scalar(q["b_size"],ivp.b_size), "[set.ivp] Could not extract 'b_size'." );
    }
    else ivp.b_size = DISOL_BLOCK_SIZE;
    ASSERT_RF( ivp.b_size > 1, "[set.ivp] Block-size should be >1." );
    return true;
}

// ------------------------------------------------------------------------

bool mx_extract_problem( const dr::mx_struct& q, HistoryProblem<double>& hip )
{
    ASSERT_RF( q, "[set.hip] Invalid input." );
    ASSERT_RF( q.has_fields({"t_start","t_end","history"}), "[set.hip] Missing field(s)." );

    ASSERT_RF( dr::mx_extract_scalar(q["t_start"],hip.t_start), "[set.hip] Could not extract 't_start'." );
    ASSERT_RF( dr::mx_extract_scalar(q["t_end"],hip.t_end), "[set.hip] Could not extract 't_end'." );

    if ( q.has_field("b_size") ) {
        ASSERT_RF( dr::mx_extract_scalar(q["b_size"],hip.b_size), "[set.hip] Could not extract 'b_size'." );
    }
    else hip.b_size = DISOL_BLOCK_SIZE;
    ASSERT_RF( hip.b_size > 1, "[set.hip] Block-size should be >1." );

    MexTimeSeries<const double,const double,false> ts;
    ASSERT_RF( ts.assign(q["history"]), "[set.hip] Could not extract history." );
    hip.ts = ts;

    ASSERT_RF( hip.ts.time().back() == hip.t_start, "The last history timepoint should correspond to t_start." );

    return true;
}

// ------------------------------------------------------------------------

bool mx_extract_properties_step( const dr::mx_struct& q, StepProperties<double>& p )
{
    ASSERT_RF( q, "[set.prop.step] Invalid input." );
    ASSERT_RF(
        q.has_field("dt") || q.has_field("nsteps") ||
        q.has_fields({"min","max"}), "[set.prop.step] Missing field(s)." );

    if ( !q.has_field("nsteps") )
    {
        if ( !q.has_fields({"min","max"}) )
        {
            double dt;
            ASSERT_RF( q.has_field("dt"), "[set.prop.step] Missing field 'dt'." );
            ASSERT_RF( dr::mx_extract_scalar(q["dt"],dt), "[set.prop.step] Could not extract 'dt'." );
            ASSERT_RF( dt > dr::c_num<double>::eps, "[set.prop.step] Time step is too small." );
            p.set_step(dt);
        }
        else
        {
            ASSERT_RF( dr::mx_extract_scalar(q["min"], p.min_step ), "[set.prop.step] Could not extract 'min'." );
            ASSERT_RF( dr::mx_extract_scalar(q["max"], p.max_step ), "[set.prop.step] Could not extract 'max'." );

            ASSERT_RF( p.min_step >= dr::c_num<double>::eps, "[set.prop.step] Min step is too small." );
            ASSERT_RF( p.max_step >= p.min_step, "[set.prop.step] Max step should be greater than min step." );

            if ( q.has_field("init") ) {
                ASSERT_RF( dr::mx_extract_scalar( q["init"], p.init_step ), "[set.prop.step] Could not extract 'init'." );
            } 
            else p.init_step = (p.min_step + p.max_step)/2;

            ASSERT_RF( p.min_step <= p.init_step, "[set.prop.step] Min step should be lesser than init step." );
            ASSERT_RF( p.max_step >= p.init_step, "[set.prop.step] Max step should be greater than init step." );
        }

        p.num_steps = 0; // used by integrators
    }
    else
    {
        ASSERT_RF( dr::mx_extract_scalar(q["nsteps"],p.num_steps), "[set.prop.step] Could not extract 'nsteps'." );
        ASSERT_RF( p.num_steps > 1, "[set.prop.step] Num steps must be greater than 1." );

        p.set_step(0); // used by integrators
    }

    return true;
}

// ------------------------------------------------------------------------

bool mx_extract_properties_error( const dr::mx_struct& q, ErrorProperties& p )
{
    ASSERT_RF( q, "[set.prop.error] Invalid input." );

    p.set_defaults();

    // Set or default
    if ( q.has_field("abs") )
    {
        ASSERT_RF( dr::mx_extract_scalar(q["abs"],p.abs_tol),
            "[set.prop.error] Could not extract 'abs'." );
    }

    if ( q.has_field("rel") )
    {
        ASSERT_RF( dr::mx_extract_scalar(q["rel"],p.rel_tol),
            "[set.prop.error] Could not extract 'rel'." );
    }

    if ( q.has_field("norm_control") )
    {
        ASSERT_RF( dr::mx_extract_scalar(q["norm_control"],p.norm_control),
            "[set.prop.error] Could not extract 'norm_control'." );
    }

    // Check values
    ASSERT_RF( p.abs_tol > dr::c_num<double>::eps, "[set.prop.error] Abs tol too small." );
    ASSERT_RF( p.rel_tol > dr::c_num<double>::eps, "[set.prop.error] Rel tol too small." );

    return true;
}

DISOL_NS_END
