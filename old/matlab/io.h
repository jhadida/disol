
//==================================================
// @title        import.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

/**
 * Heavy stuff to extract properties from Mex inputs.
 */



        /********************     **********     ********************/
        /********************     **********     ********************/



DISOL_NS_START

bool mx_extract_problem( const dr::mx_struct& q, InitialValueProblem<double>& ivp );
bool mx_extract_problem( const dr::mx_struct& q, HistoryProblem<double>& hip );

// ------------------------------------------------------------------------

template <class _idata, class _metric>
bool mx_extract_properties( const dr::mx_struct& q, Properties<_idata,_metric>& p )
{
    ASSERT_RF( q, "[set.prop] Invalid input." );
    ASSERT_RF( q.has_fields({"step","error"}), "[set.prop] Missing field(s)." );

    dr::mx_struct cfg;

    ASSERT_RF( cfg.wrap( q["step"] ), "[set.prop] Couldn't extract 'step'." );
    ASSERT_RF( mx_extract_properties_step(cfg,p.step), "[set.prop] Couldn't build step." );

    ASSERT_RF( cfg.wrap( q["error"] ), "[set.prop] Couldn't extract 'error'." );
    ASSERT_RF( mx_extract_properties_error(cfg,p.error), "[set.prop] Couldn't build error." );

    return true;
}

// ------------------------------------------------------------------------

bool mx_extract_properties_step  ( const dr::mx_struct& q, StepProperties<double>& p );
bool mx_extract_properties_error ( const dr::mx_struct& q, ErrorProperties& p );

// ------------------------------------------------------------------------

template <class TS>
mxArray* mx_export_timeseries( const TS& ts, usize_t ntimes, usize_t nsignals )
{
    typedef typename TS::value_type _value;
    typedef typename TS::time_type  _time;
    MexTimeSeries<_value,_time,false> mts;

    // allocate time-series
    ASSERT_RVAL( mts.create( ntimes, nsignals ), nullptr, "Could not allocate time-series." );

    // copy data
    for ( usize_t i = 0; i < ntimes; ++i )
    {
        mts.time (i) =      ts.time (i)  ;
        mts.state(i) .copy( ts.state(i) );
    }

    return mts.mx();
}

template <class TS>
mxArray* mx_export_timeseries( const TS& ts )
    { return mx_export_timeseries( ts, ts.n_times(), ts.n_signals() ); }

template <class V, class T>
mxArray* mx_export_dspool( const PoolDownsampler<V,T>& ds )
{
    // output dimensions
    ASSERT_RVAL( ds.active() && !ds.empty(), nullptr, "Invalid downsampler." );
    usize_t ntimes   = ds.length;
    usize_t nsignals = ds.dspool.front().state_size();

    // allocate output
    MexTimeSeries<V,T,false> mts;
    ASSERT_RVAL( mts.create( ntimes, nsignals ), nullptr, "Could not allocate time-series." );

    usize_t k = 0;
    for ( auto& b: ds.dspool )
    for ( usize_t i = 0; i < b.size(); ++i, ++k )
    {
        mts.time (k) =      b.time (i)  ;
        mts.state(k) .copy( b.state(i) );
    }

    return mts.mx();
}

// ------------------------------------------------------------------------

template <class V, class T>
bool mx_import_timeseries( const dr::mx_struct& mts, TimeSeries<V,T>& ts )
{
    using ts_type  = TimeSeries<V,T>;
    using vec_time = typename ts_type::vec_time;
    using mat_vals = typename ts_type::mat_vals;

    // check input
    ASSERT_RF( mts, "Invalid input structure." );
    ASSERT_RF( mts.has_fields({"time","vals"}),
        "Input structure should have fields 'time' and 'vals'." );

    // extract time and vals
    vec_time t; mat_vals v;
    ASSERT_RF( dr::mx_extract_vector( mts["time"], t ), "Could not extract field 'time'." );
    ASSERT_RF( dr::mx_extract_matrix( mts["vals"], v ), "Could not extract field 'vals'." );

    // assign time-series to compute associated slopes
    ASSERT_RF( ts.assign( t, v ), "Could not assign time-series." );
    return true;
}

template <class V, class T>
bool mx_import_timecourse( const dr::mx_struct& mtc, TimeCourse<V,T>& tc )
{
    using tc_type  = TimeCourse<V,T>;
    using vec_time = typename tc_type::vec_time;
    using vec_vals = typename tc_type::vec_vals;

    // check input
    ASSERT_RF( mtc, "Invalid input structure." );
    ASSERT_RF( mtc.has_fields({"time","vals"}),
        "Input structure should have fields 'time' and 'vals'." );

    // extract time and vals
    vec_time t; vec_vals v;
    ASSERT_RF( dr::mx_extract_vector( mtc["time"], t ), "Could not extract field 'time'." );
    ASSERT_RF( dr::mx_extract_vector( mtc["vals"], v ), "Could not extract field 'vals'." );

    // assign time-series to compute associated slopes
    ASSERT_RF( tc.assign( t, v ), "Could not assign time-course." );
    return true;
}

DISOL_NS_END
