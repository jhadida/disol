

// plugin_clamp
bool configure( const dr::mx_struct& cfg )
{
    DRAYN_WASSERT_RF( cfg, "[plugin.clamp] Invalid input." );
    DRAYN_WASSERT_RF( cfg.has_fields({"lower","upper"}), "[plugin.clamp] Missing field(s)." );

    DRAYN_WASSERT_RF( dr::mx_extract_scalar(cfg["lower"],lower),
        "[plugin.clamp] Could not extract 'lower'." );
    DRAYN_WASSERT_RF( dr::mx_extract_scalar(cfg["upper"],upper),
        "[plugin.clamp] Could not extract 'upper'." );

    return true;
}

// plugin randn
bool configure( const dr::mx_struct& cfg )
{
    DRAYN_WASSERT_RF( cfg, "[plugin.white_noise] Invalid input." );
    DRAYN_WASSERT_RF( cfg.has_field("sigma"), "[plugin.white_noise] Missing field 'sigma'." );

    value_type sigma;
    DRAYN_WASSERT_RF( dr::mx_extract_scalar(cfg["sigma"],sigma),
        "[plugin.white_noise] Could not extract 'sigma'." );

    return configure(sigma);
}
