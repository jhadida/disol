
//==================================================
// @title        timeseries.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <memory>



        /********************     **********     ********************/
        /********************     **********     ********************/



DISOL_NS_START

template <class _value, class _time, bool _managed = false>
class MexTimeSeries
    : public TimeSeries< _value, _time >
{
public:

    typedef _value     value_type;
    typedef _time      time_type;

    using parent       = TimeSeries< _value, _time >;
    using traits       = dr::mx_traits< _value, _managed >;
    using mx_ptr_type  = typename traits::mx_ptr_type;
    using mx_shared    = std::shared_ptr<mx_ptr_type>;
    using vec_time     = typename parent::vec_time;
    using mat_vals     = typename parent::mat_vals;

    // ----------  =====  ----------

    MexTimeSeries()
        { clear(); }

    MexTimeSeries( mx_ptr_type *pm )
        { ASSERT_R( assign(pm), "Could not build MexTimeSeries object." ); }

    MexTimeSeries( usize_t ntimes, usize_t nsignals )
        { ASSERT_R( create(ntimes,nsignals), "Could not build MexTimeSeries object." ); }

    ~MexTimeSeries()
        { clear(); }

    // assignment methods
    bool assign( mx_ptr_type *pm );
    bool create( usize_t ntimes, usize_t nsignals );

    // force the instance to free the allocated Matlab structure (even if managed is false)
    bool force_mx_release();

    // overloads
    virtual void clear()
        { parent::clear(); m_struct.reset(); }
    virtual bool valid() const
        { return static_cast<bool>(m_struct) && parent::valid(); }

    // access to underlying Mex structure
    inline mx_ptr_type* mx() const
        { return m_struct.get(); }

protected:

    mx_shared  m_struct;
};

// ------------------------------------------------------------------------

template <class V, class T, bool M>
bool MexTimeSeries<V,T,M>::assign( mx_ptr_type *pm )
{
    // clear everything first
    clear();

    // wrap input struct
    dr::mx_struct input;
    ASSERT_RF( input.wrap(pm), "Could not wrap input structure." );
    ASSERT_RF( input.has_fields({"time","vals"}),
        "Input structure should have fields 'time' and 'vals'." );

    // extract time and vals
    vec_time t; mat_vals v;
    ASSERT_RF( dr::mx_extract_vector( input["time"], t ), "Could not extract field 'time'." );
    ASSERT_RF( dr::mx_extract_matrix( input["vals"], v ), "Could not extract field 'vals'." );

    // assign parent to computed associated slopes
    ASSERT_RF( parent::assign( t, v ), "Could not assign parent class." );

    // remember the mex pointer
    typedef typename traits::deleter_type del_type;
    m_struct.reset( pm, del_type() );

    return true;
}

// ------------------------------------------------------------------------

template <class V, class T, bool M>
bool MexTimeSeries<V,T,M>::create( usize_t ntimes, usize_t nsignals )
{
    ASSERT_RF( ntimes*nsignals > 0, "Size cannot be 0." );

    // create the structure
    mxArray *mxs = dr::mx_create_struct({"time","vals"});

    // assign fields
    mxSetField( mxs, 0, "time",
        dr::allocator_matlab_numeric<T>::allocate_col(ntimes) );
    mxSetField( mxs, 0, "vals",
        dr::allocator_matlab_numeric<V>::allocate_matrix(ntimes,nsignals) );

    // assign to self
    if ( !assign(mxs) )
    {
        DRAYN_WARN( "Could not create the time-series." );
        mxDestroyArray(mxs); return false;
    }
    else return true;
}

// ------------------------------------------------------------------------

template <class V, class T, bool M>
bool MexTimeSeries<V,T,M>::force_mx_release()
{
    if ( !valid() )
        return true;

    ASSERT_RF( m_struct.unique(), "This resource is currently being shared, can't release it!" );

    if ( !M )
    {
        mx_ptr_type *p = m_struct.get();
        clear(); mxDestroyArray(p);
    }
    else clear();

    return true;
}

DISOL_NS_END
