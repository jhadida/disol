
//==================================================
// @title        controller.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

/**
 * Controllers are responsible for rescaling the time-step depending on the error
 * estimate at each step. They can have an arbitrary long memory, and algorithms
 * usually borrow from control theory in order to stabilize the variations.
 *
 * Note that controllers expect a *SCALED* error, so it's important to know what
 * variation you expect on each state variable, and to define your relative and
 * absolute tolerances accordingly (see properties.Error).
 *
 * Both methods "check" and "scaling_factor" should be called only once per step
 * trial, because they typically record information about the input error and
 * rejection status in order to inform future decisions.
 *
 * The method scaling_factor should be called at the end of each step trial to get
 * a multiplying-factor for the current timestep based on the estimated error.
 * The algorithms that compute this factor typically use past error estimates
 * as well as the current acceptance/rejection status. Note that the time-step may
 * be changed (ie, scaling factor != 1) even though the step is accepted (eg for
 * increasing the timestep in smooth areas).
 *
 * After each call to the scaling_factor method, the method check should be called
 * in order to validate the current step trial, depending on the estimated error.
 * If the error is not acceptable, then another trial with an adapted timestep
 * (according to the previous recommendation) should be run. This cycle continues
 * until either a trial step is accepted or typically a maximum number of trials
 * is exceeded (see default controller for a detailed example).
 *
 * Check integrator/adaptive_step for an example.
 */



        /********************     **********     ********************/
        /********************     **********     ********************/



DISOL_NS_START

struct controller
{
    inline bool accept( double err ) const { return err <= 1.0; }
    inline bool reject( double err ) const { return !accept(err); }

    virtual bool   update         ( double err ) =0;
    virtual double scaling_factor ( double err ) =0;
};

DISOL_NS_END
