
//==================================================
// @title        integrator.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

/**
 * integrator objects propagate the system between two time instants, typically
 * given an initial state. At the moment, only Initial Value Problems are supported.
 *
 * integrators may or may not leverage the fact that the underlying stepper provides
 * an estimation of its error to locally adapt the time-step.
 *
 * They essentially serve as an intermediary between the system that can compute the
 * state and derivatives at a given timepoint, and the stepper that propagates the
 * state of the system from one timepoint to the next.
 */



        /********************     **********     ********************/
        /********************     **********     ********************/



DISOL_NS_START

template <class S>
class integrator
{
public:

    using stepper_type   = S;
    using time_type      = typename stepper_type::time_type;
    using value_type     = typename stepper_type::value_type;
    using system_type    = typename stepper_type::system_type;
    using property_type  = typename stepper_type::property_type;
    using handle_type    = typename stepper_type::handle_type;
    using problem_type   = typename handle_type::problem_type;

    // ----------  =====  ----------

    // Store the stepper as a public member
    stepper_type stepper;

    integrator()
        { clear(); }

    inline void clear() {
        m_handle.clear();
        stepper.clear();
    }

    virtual bool is_adaptive() const =0;
    inline bool is_fixed_step() const { return !is_adaptive(); }

    virtual void integrate( system_type& sys, problem_type& pb, property_type& prop ) =0;


protected:

    // Data used to propagate the system during integration
    handle_type m_handle;

    // Initialise the system before integration
    bool init( system_type& sys, problem_type& pb, property_type& prop );

    /**
     * Fetch is called before a step and, if the step is accepted by the integrator (cf error control),
     * it is then committed into the records.
     *
     * This means that while the stepper is at work, the latest timestep in the pool (aka "next" in handle)
     * is one step ahead of the "present" (aka cur.t). This is important to know if you need to interpolate
     * past records to compute e.g. delayed terms, because the last timepoint in the records should be ignored.
     * Hence the .lag() method implemented in dr::timepool, and used by default in LSBM_Helper when initialising memory.
     */
    void commit_next() {
        pool_commit_timepoint( m_handle.records(), m_handle.next );
        m_handle.commit();
    }

    void fetch_next() {
        interrupt_throw( "Keyboard interruption.", DISOL_INTERRUPT_N );
        m_handle.records().increment();
        pool_fetch_timepoint( m_handle.records(), m_handle.next );
    }

};

// ------------------------------------------------------------------------

template <class S>
bool integrator<S>::init( system_type& sys, problem_type& pb, property_type& prop )
{
    // Quick check-up of inputs
    DRAYN_ASSERT_RF( prop.check(),
        "[properties] Invalid properties." );
    DRAYN_ASSERT_RF( pb.tspan() >= dr::c_num<time_type>::eps,
        "[problem] Time-interval is too small." );

    // Initialise memory pool
    //  - resize the pool to the dimensions of inital state(s)
    //  - record initial conditions depending on problem to solve
    //  - make sure initial state-size and system state-size match
    DRAYN_ASSERT_RF( pb.init( sys.records, !is_adaptive() ), "[records] Could not initialise memory pool." );
    DRAYN_ASSERT_RF( pb.ndims() == sys.ndims(), "[system] Size mismatch between initial conditions and system state-size." );

    // Initialise integration data
    //  - bind to system and problem being solved
    //  - allocate timepoints
    DRAYN_ASSERT_RF( m_handle.init( sys, pb, !is_adaptive() ), "[handle] Failed to initialise." );

    // Initialise the stepper
    //  - bind "current" timepoint to the last timepoint in records
    //  - call system to set its derivative (and Jacobian if needed)
    DRAYN_ASSERT_RF( stepper.init( m_handle, prop ), "[stepper] Failed to initialise." );

    return true;
}

DISOL_NS_END
