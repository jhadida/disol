
//==================================================
// @title        plugin.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

/**
 * plugins are built to listen to integrator events (see properties.prop_event).
 * This is useful typically when constraints must be enforced on the states
 * independently from the state equations (eg, range constraints), or when
 * intermediary or auxiliary information must be saved, eg for monitoring or
 * analysis purposes.
 */


        /********************     **********     ********************/
        /********************     **********     ********************/



DISOL_NS_START

template <class H>
class plugin
{
public:

    using handle_type  = H;
    using event_type   = prop_event<handle_type>;
    using system_type  = typename handle_type::system_type;
    using signal_type  = typename event_type::signal_type;
    using slot_type    = typename event_type::slot_type;

    // ----------  =====  ----------

    plugin() {}

    inline void attach( signal_type& sig ) {
        m_slot = sig.subscribe(
            [this]( handle_type& s ){ this->callback(s); }
        );
    }
    inline void detach( signal_type& sig )
        { sig.unsubscribe( m_slot ); }

protected:

    // The callback function is called at each publishing event
    virtual void callback( handle_type& handle ) =0;

    // The slot is waiting for publications on the attached channel
    slot_type m_slot;
};

DISOL_NS_END
