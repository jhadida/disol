
//==================================================
// @title        stepper.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

/**
 * stepper objects implement the various mathematical and numerical approaches to
 * computing a single time-step in dynamical systems described by differences (eg
 * Euler, Midpoint, Runge-Kutta, etc.).
 *
 * If/when possible, the stepper may be able to estimate the error of its prediction,
 * in which case it should be returned by the step function. Otherwise, the output of
 * the step function can be anything, typically 0.0.
 *
 * Finally, some steppers may also suggest an optimal timestep to integrator objects
 * in addition to / instead of the time-step suggested by the controller. This can be
 * done via the timepoint field state.next.dt. In either case, the integrator should
 * have the final word with regards to the chosen time-step.
 */

#include <cstdint>
#include <type_traits>



        /********************     **********     ********************/
        /********************     **********     ********************/



DISOL_NS_START

template <
    class    S, // (cf system): implements the derivative & Jacobian
    uint8_t  ord,  // maximum derivative order used for stepping
    bool     err = false,  // whether the stepper estimates prediction error
    bool     jac = false, // whether it requires the Jacobian information
    class    M = default_metric // metric used for error control
>
class stepper
{
public:

    static const uint8_t order = ord;

    using estimates_error = dr::bool_type<err>;
    using uses_jacobian   = dr::bool_type<jac>;

    using system_type = S;
    using metric_type = M;
    
    using time_type   = typename system_type::time_type;
    using value_type  = typename system_type::value_type;
    using state_type  = typename system_type::state_type;
    using deriv_type  = typename system_type::deriv_type;
    using array_type  = typename system_type::array_type;

    // Following types may be overriden depending on the needs of the stepper
    using timepoint_type = typename std::conditional< jac,
            timepoint_jac< value_type, time_type >,
            timepoint< value_type, time_type >
        >::type;

    using handle_type   = handle< system_type, timepoint_type >;
    using property_type = properties< handle_type, metric_type >;

    // ----------  =====  ----------

    // Clear contents
    void clear() {}

    // Set initial derivative
    // NOTE: overload this method to set the Jacobian if needed
    bool init( handle_type& handle, property_type& prop )
    {
        auto& tp  =  handle.cur;
        auto& sys = *handle.sys;

        pool_fetch_timepoint( sys.records, tp );
        tp.dt = prop.step.init_step;
        sys.derivative( tp.t, tp.x, tp.dxdt );

        return true;
    }

    // Stepping method to be overloaded, returns error estimate (or 0)
    virtual double step( handle_type& handle, property_type& prop ) =0;

    // Smallest substep ratio
    inline double smallest_substep() const { return 1.0; }

};

DISOL_NS_END
