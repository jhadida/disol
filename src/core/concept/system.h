
//==================================================
// @title        system.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

/**
 * system objects implement methods to evaluate the rhs of an ODE system in canonical
 * form (assuming the system can be reduced to a set of first order equations only).
 *
 * If/when necessary, the system may also provide a implementation of the Jacobian,
 * either using the analytical formulation or a numerical approximation. By default,
 * a computationally expensive central approximation is used to compute the Jacobian.
 *
 * Any realistic system will finally come with parameters, which may alter the values
 * or even the form of the equations. Make sure your system is fully parameterized
 * before you give it to the integrator for solving your initial value problem, by
 * defining your own configuration methods in the derived classes.
 */



        /********************     **********     ********************/
        /********************     **********     ********************/



DISOL_NS_START

template <class V, class T>
struct system_base
{
    DISOL_TRAITS(V,T)

    // Public access to the records
    pool_type records;

    inline void clear() { records.clear(); }

    inline uidx_t size() const { return records.size(); } // # of recorded states
    inline uidx_t ndims() const { return records.ndims(); } // # of coordinates per state
};

// ------------------------------------------------------------------------

template <class V, class T = double, bool sto = false>
struct system: public system_base<V,T>
{
    DISOL_TRAITS(V,T)

    using is_stochastic = dr::bool_type<sto>;

    /**
     * Canonical systems must provide a method to compute the derivative at a given timepoint.
     *
     * Input timepoint members  : t, x
     * Output timepoint members : dxdt
     */
    virtual void derivative( time_type t, const state_type& x, const array_type& dxdt ) const =0;

    /**
     * All systems must provide a method to compute the Jacobian at a given timepoint.
     * By default, the NumericJacobian is used to compute each element of the Jacobian
     * numerically, using a forward approximation to compute partial derivatives, which 
     * is slow (requires n evaluations of the derivative). Central approximation can be
     * used for increased accuracy, but it is even slower (2n evaluations of the derivative).
     *
     * If you are using a stepping scheme that requires the Jacobian, try to provide
     * your own implementation for computing it (eg derived from analytics).
     *
     * Input timepoint members  : t, x
     * Output timepoint members : dfdx (matrix), dfdt (vector)
     */
    void jacobian( time_type t, const state_type& x, const jacob_type& dfdx, const array_type& dfdt ) const {
        dr::fill( dfdt, value_type(0) );
        m_numjac( *this, t, x, dfdx );
    }

protected:

    mutable NumericJacobian<value_type,time_type> m_numjac;
};

// ------------------------------------------------------------------------

template <class V, class T = double>
struct system_ito: public system<V,T,true>
{
    DISOL_TRAITS(V,T)
    
    /**
     * Ito systems are of the form:
     *   dX = Drift(t,X) dt + Volatility(t,X) dW
     *
     * Where W is a Wiener process.
     */
    virtual void drift( time_type t, const state_type& x, const array_type& dxdt ) const =0;
    virtual void volatility( time_type t, const state_type& x, const array_type& dxdw ) const =0;

    // By default the derivative returns the drift component
    inline void derivative( time_type t, const state_type& x, const array_type& dxdt ) const
        { drift( t, x, dxdt ); }
};

DISOL_NS_END
