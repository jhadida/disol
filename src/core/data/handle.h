
//==================================================
// @title        handle.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <stdexcept>



        /********************     **********     ********************/
        /********************     **********     ********************/



DISOL_NS_START

template <class S, class TP>
struct handle
{
    using tp_type       = TP;
    using system_type   = S;
    using time_type     = typename system_type::time_type;
    using value_type    = typename system_type::value_type;
    using pool_type     = typename system_type::pool_type;
    using problem_type  = problem_base< value_type, time_type >;

    // ----------  =====  ----------

    system_type    *sys;
    problem_type   *prob;           
    tp_type         cur, next;      // current and next timepoints
    double          err;            // error estimate for last step
    bool            fix;            // fixed-step integration

    // ----------  =====  ----------

    // bind input system and allocate timepoint storage
    bool init( system_type& s, problem_type& p, bool fixed_step );

    void clear();
    void commit(); // copy non-allocated timepoint data from next to cur
    void adjust_step(); // adjust current time-step to finish exactly at tend

    // True if cur.t is past the last time in the given problem
    inline bool done() const { return cur.t >= prob->tend; }

    // Validity of the current state
    inline bool valid() const
    {
        return sys && prob && err >= 0.0 &&
            cur.size() == next.size() &&
            cur.size() == sys->ndims();
    }

    // Proxy to system properties
    inline uidx_t size() const { return sys? sys->ndims() : 0; }
    inline pool_type& records() const { return sys->records; }

    // Proxy to problem properties
    inline time_type tstart() const { return prob->tstart; }
    inline time_type tend() const { return prob->tend; }
    inline time_type tspan() const { return tend()-tstart(); }

};



    //--------------------     ==========     --------------------//
    //--------------------     **********     --------------------//



template <class S, class T>
void handle<S,T>::clear()
{
    sys  = nullptr;
    prob = nullptr;
    err  = 0.0;
    fix  = false;

    cur  .clear();
    next .clear();
}

// ------------------------------------------------------------------------

template <class S, class T>
bool handle<S,T>::init( system_type& s, problem_type& p, bool fixed_step )
{
    sys  = &s;
    prob = &p;
    err  = 0.0;
    fix  = fixed_step;

    // Allocate storage for timepoints
    cur  .resize( s.ndims() );
    next .resize( s.ndims() );

    return valid();
}

// ------------------------------------------------------------------------

template <class S, class T>
void handle<S,T>::commit()
{
    // Copy only non-allocated data
    cur.t     = next.t;
    cur.dt    = next.dt;
    cur.x     = next.x; // "pointer" to pool vector
    cur.index = next.index;
}

// ------------------------------------------------------------------------

template <class S, class T>
void handle<S,T>::adjust_step()
{
    const time_type dt = prob->tend - cur.t;
    DRAYN_ASSERT_ERR( dt > 0, "Overshot the last timepoint." )

    if ( cur.dt > dt ) {
        DRAYN_INFO( "Adjusting time-step to finish at the last timepoint." )
        cur.dt = dt;
    }
}

DISOL_NS_END
