
//==================================================
// @title        problem.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

/**
 * Initial value problems are a type of integration problem given a function
 * to integrate (here a system capable of computing its derivative) and an
 * initial state of the system.
 *
 * This data structure simply translates to: "integrate a given system between
 * times t0 and t1 assuming that the state of the system at t0 is x0".
 */



        /********************     **********     ********************/
        /********************     **********     ********************/



DISOL_NS_START

template <class V, class T>
struct problem_base
{
    DISOL_TRAITS(V,T)

    time_type tstart, tend;
    uidx_t    bsize; // block-size for memory pool

    // ----------  =====  ----------

    problem_base()
        : bsize(DISOL_BLOCK_SIZE) {}

    // Time-interval for integration
    inline time_type tspan() const { return tend-tstart; }

    // Time-interval and number of states in history prior to integration
    virtual time_type hspan() const =0;
    virtual uidx_t hsize() const =0;

    // What is the size of the problem?
    virtual uidx_t ndims() const =0;

    // Initialise records before integration
    virtual bool init( pool_type& records, bool fixed ) const =0;
};

// ------------------------------------------------------------------------

template <class V, class T = double>
struct problem_ivp: public problem_base<V,T>
{
    DISOL_TRAITS(V,T)
    using vec_type = pvec<const V>;

    vec_type xstart; 

    // ----------  =====  ----------

    inline uidx_t ndims() const { return xstart.size(); }
    inline time_type hspan() const { return 0; }
    inline uidx_t hsize() const { return 1; }

    bool init( pool_type& records, bool fixed ) const
    {
        // resize memory pool
        records.init( ndims(), this->bsize, fixed );
        DRAYN_ASSERT_RF( records, "[problem_ivp.init] Memory pool initialisation failed." )

        // record initial time and state
        records.time () = this->tstart;
        records.state() .copy(xstart);

        return true;
    }
};

// ------------------------------------------------------------------------

template <class V, class T = double>
struct problem_hist: public problem_base<V,T>
{
    DISOL_TRAITS(V,T)
    using ts_type = ts_ptr<const V, const T>;

    ts_type ts;

    // ----------  =====  ----------

    inline uidx_t ndims() const { return ts.ndims(); }
    inline time_type hspan() const { return ts.tspan(); }
    inline uidx_t hsize() const { return ts.ntime(); }

    bool init( pool_type& records, bool fixed ) const
    {
        // resize memory pool
        records.init( ndims(), this->bsize, fixed );
        DRAYN_ASSERT_RF( records, "[problem_hist.init] Memory pool initialisation failed." )

        // TODO: use state iteration on ts_shr, and check how it is initialised
        for ( uidx_t i = 0; i < ts.size(); ++i )
        {
            records.time () = ts.time(i);
            records.state() .copy( ts.state(i) );
            records.increment();
        }
        records.decrement();

        return true;
    }
};

DISOL_NS_END
