
//==================================================
// @title        parameters.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <tuple>



        /********************     **********     ********************/
        /********************     **********     ********************/



DISOL_NS_START

template <class T>
struct prop_error
{
    using error_type = T;

    error_type  abs_tol, rel_tol;
    bool        norm_control;

    prop_error() { set_defaults(); }

    inline void set_tolerance( error_type abs, error_type rel )
        { abs_tol=abs; rel_tol=rel; }
    inline void set_defaults()
        { abs_tol=1e-8; rel_tol=1e-6; norm_control=false; }
};

// ------------------------------------------------------------------------

template <class T>
struct prop_step
{
    using time_type = T;

    time_type  init_step, min_step, max_step;
    uidx_t     num_steps;

    inline void set_step( time_type dt )
        { set_step( dt, dt/100, dt*100 ); }
    inline void set_step( time_type dt, time_type dtmin, time_type dtmax )
        { init_step=dt; min_step=dtmin; max_step=dtmax; }
    inline void set_num_steps( uidx_t n )
        { num_steps = n; }
};

// ------------------------------------------------------------------------

template <class H>
struct prop_event
{
    using handle_type = H;
    using signal_type = dr::signal<handle_type>;
    using slot_type   = dr::slot<handle_type>;
    
    signal_type
        after_init, before_step, after_step, after_commit;
};

// ------------------------------------------------------------------------

/**
 * The metric is used for error-control (default: disol::default_metric).
 */
template <class H, class M>
struct properties
{
    using handle_type  = H;
    using metric_type  = M;
    using time_type    = typename handle_type::time_type;
    using error_type   = double;

    prop_step<time_type>     step;
    prop_error<error_type>   error;
    prop_event<handle_type>  event;
    metric_type              metric;

    bool check() const
    {
        const time_type eps = dr::c_num<time_type>::eps;
        DRAYN_ASSERT_RF( step.min_step >= eps || step.num_steps > 0,
            "[prop.step] Min step is too small." );

        DRAYN_ASSERT_RF( step.init_step >= step.min_step,
            "[prop.step] Initial step should be greater than min step." );

        DRAYN_ASSERT_RF( step.max_step >= step.init_step,
            "[prop.step] Max step should be greater than initial step." );

        DRAYN_ASSERT_RF( error.abs_tol > eps,
            "[prop.error] Absolute tolerance is too small." );

        DRAYN_ASSERT_RF( error.rel_tol > eps,
            "[prop.error] Relative tolerance is too small." );

        return true;
    }
};

DISOL_NS_END
