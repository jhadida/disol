
//==================================================
// @title        timepoint.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

/**
 * A timepoint is a data-structure that stores the state of the system x and
 * its derivative with respect to time dxdt evaluated at a given time t.
 *
 * The timepoint can also store the Jacobian J of a system if needed, and stores
 * the timestep dt that was used when the derivative/Jacobian were computed, as
 * well as the index of that time-point in the memory pool.
 */


        /********************     **********     ********************/
        /********************     **********     ********************/



DISOL_NS_START

template <class V, class T = double>
struct timepoint
{
    DISOL_TRAITS(V,T)

    uidx_t      index;
    time_type   t, dt;
    state_type  x;
    deriv_type  dxdt;

    // ----------  =====  ----------

    timepoint() { clear(); }

    explicit
    timepoint( uidx_t s ) { resize(s); }

    void clear() {
        index = 0; 
        t = dt = 0.0;
        // x = state_type();
        // dxdt = deriv_type();
    }

    inline void resize( uidx_t s )
        { dxdt.resize(s,0); }

    inline uidx_t ndims() const { return dxdt.size(); }
    inline uidx_t size() const { return ndims(); }

    // WARNING
    //   The following methods are implemented for convenience.
    //   They assume a fixed timestep dt; if an adaptive timestep integrator is used,
    //   there will be a differente between these methods and the actual previous/next
    //   times saved in the memory pool.
    inline time_type prev_time() const { return t - dt; }
    inline time_type next_time() const { return t + dt; }
};

// ------------------------------------------------------------------------

template <class V, class T = double>
struct timepoint_jac: public timepoint<V,T>
{
    DISOL_TRAITS(V,T)
    using parent = timepoint<V,T>;

    jacob_type  dfdx; // Jacobian matrix
    deriv_type  dfdt; // non-stationarity

    // ----------  =====  ----------

    timepoint_jac() { clear(); }

    explicit
    timepoint_jac( uidx_t s ) { resize(s); }

    void clear() {
        parent::clear();
        // dfdx.clear();
        // dfdt.clear();
    }

    void resize( uidx_t s ) {
        parent::resize(s);
        dfdt.resize(s,0);
        if ( ! dfdx.chksize(s,s) )
            dfdx = dr::make_mat<value_type>(s,s);
    }
};

// ------------------------------------------------------------------------

/**
 * fetch  : fetch the latest/a given pool record and bind the input timepoint
 * commit : copy input timepoint to the corresponding record
 */

template <class P, class TP>
void pool_fetch_timepoint( const P& pool, uidx_t idx, TP& tp )
{
    DRAYN_DASSERT_R( idx <= pool.index(), "Index out of range." )
    DRAYN_DASSERT_R( tp.ndims() == pool.ndims(), "Dimensions mismatch." )

    tp.index = idx;
    tp.t = pool.time(idx);
    tp.x = pool.state(idx);
}

template <class P, class TP>
inline void pool_fetch_timepoint( const P& pool, TP& tp )
    { pool_fetch_timepoint( pool, pool.index(), tp ); }

// ----------  =====  ----------

template <class P, class TP>
void pool_commit_timepoint( const P& pool, const TP& tp )
{
    const uidx_t idx = tp.index;

    DRAYN_DASSERT_R( idx <= pool.index(), "Index out of range." );
    DRAYN_DASSERT_R( tp.ndims() == pool.ndims(), "Dimensions mismatch." );

    // Nothing to do for state, because using reference array
    pool.time(idx) = tp.t;
}

DISOL_NS_END
