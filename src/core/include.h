#ifndef DISOL_CORE_H_INCLUDED
#define DISOL_CORE_H_INCLUDED

//==================================================
// @title        include.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

// data
#include "data/problem.h"
#include "data/timepoint.h"
#include "data/handle.h"
#include "data/properties.h"

// concepts
#include "concept/system.h"
#include "concept/stepper.h"
#include "concept/integrator.h"
#include "concept/controller.h"
#include "concept/plugin.h"

#endif
