#ifndef DISOL_H_INCLUDED
#define DISOL_H_INCLUDED

//==================================================
// @title        disol.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#define DISOL_NS_START namespace disol {
#define DISOL_NS_END   }

#include "drayn.h"

// ----------  =====  ----------

#ifndef DISOL_BLOCK_SIZE
#define DISOL_BLOCK_SIZE 1000
#endif

// only check for process interruption every 1000 steps
// NOTE:
//  requires -lut as compilation flag
//  disable by setting to 0
#ifndef DISOL_INTERRUPT_N
#define DISOL_INTERRUPT_N 1000
#endif

// ----------  =====  ----------

// propagate flags from Drayn
#ifdef DRAYN_USE_JMX
    #ifndef DISOL_USE_JMX
    #define DISOL_USE_JMX
    #endif 
#endif

#ifdef DRAYN_USE_ARMA
    #ifndef DISOL_USE_ARMA
    #define DISOL_USE_ARMA
    #endif 
#endif

// ------------------------------------------------------------------------

#include "traits.h"

#include "utils/include.h"
#include "core/include.h"
#include "integrator/include.h"
#include "plugin/include.h"
#include "stepper/include.h"
#include "external/include.h"

#endif
