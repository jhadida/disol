#ifndef DISOL_EXTERNAL_H_INCLUDED
#define DISOL_EXTERNAL_H_INCLUDED

//==================================================
// @title        external
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#ifdef DISOL_USE_JMX
    #include "matlab.h"
#endif

#endif
