
//==================================================
// @title        matlab.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include "jmx.h"
#include <type_traits>
#include <cmath>



        /********************     **********     ********************/
        /********************     **********     ********************/



DISOL_NS_START

template <class Ctn, class T = typename Ctn::value_type>
pvec<T> _jmx_to_pvec( const Ctn& in ) 
    { return pvec<T>( in.memptr(), in.numel() ); }

template <class Mat, class T = typename Mat::value_type>
pmat<T> _jmx_to_pmat( const Mat& in ) 
    { return pmat<T>( _jmx_to_pvec(in), in.nrows(), in.ncols() ); }

template <class Ctn, class T = typename Ctn::value_type>
svec<T> _jmx_to_svec( const Ctn& in ) 
    { return svec<T>().copy(_jmx_to_pvec(in)); }

template <class Mat, class T = typename Mat::value_type>
smat<T> _jmx_to_smat( const Mat& in ) 
    { return smat<T>( _jmx_to_svec(in), in.nrows(), in.ncols() ); }

// ------------------------------------------------------------------------

template <class V, class T>
bool jmx_import( const jmx::Struct& in, problem_ivp<V,T>& out );

template <class V, class T>
bool jmx_import( const jmx::Struct& in, problem_hist<V,T>& out );

template <class H, class M>
bool jmx_import( const jmx::Struct& in, properties<H,M>& out );

template <class T>
bool jmx_import( const jmx::Struct& in, prop_error<T>& out );

template <class T>
bool jmx_import( const jmx::Struct& in, prop_step<T>& out );

// ------------------------------------------------------------------------

template <class V, class T>
bool jmx_import( const jmx::Struct& in, ts_shr<V,T>& out );

template <class V, class T>
bool jmx_import( const jmx::Struct& in, ts_ptr<V,T>& out );

template <class V, class T>
bool jmx_import( const jmx::Struct& in, tc_shr<V,T>& out );

template <class V, class T>
bool jmx_import( const jmx::Struct& in, tc_ptr<V,T>& out );

// ------------------------------------------------------------------------

template <class V, class T>
bool jmx_export( const ts_shr<V,T>& in, jmx::Struct out );

template <class V, class T>
bool jmx_export( const ts_ptr<V,T>& in, jmx::Struct out );

template <class V, class T>
bool jmx_export( const tc_shr<V,T>& in, jmx::Struct out );

template <class V, class T>
bool jmx_export( const tc_ptr<V,T>& in, jmx::Struct out );

template <class V, class T>
bool jmx_export( const dr::timepool<V,T>& in, jmx::Struct out );

// ------------------------------------------------------------------------

template <class V, class T>
ts_ptr<V,T> jmx_make_ts( jmx::Struct out, uidx_t nchan, T tstart, T tend, T step );

template <class V, class T>
tc_ptr<V,T> jmx_make_tc( jmx::Struct out, T tstart, T tend, T step );

// ------------------------------------------------------------------------

#include "matlab.hpp"

DISOL_NS_END
