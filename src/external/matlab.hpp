
template <class V, class T>
bool jmx_import( const jmx::Struct& in, problem_ivp<V,T>& out )
{
    JMX_WASSERT_RF( in.has_fields({ "tstart", "tend", "xstart" }), "[import.ivp] Missing field(s)." )

    out.tstart = in.getnum<T>("tstart");
    out.tend = in.getnum<T>("tend");

    out.bsize = in.getnum<uidx_t>("bsize",DISOL_BLOCK_SIZE);
    JMX_WASSERT_RF( out.bsize > 1, "[import.ivp] Block-size should be >1." )

    out.xstart = _jmx_to_pvec(in.getvec<V>("xstart"));
    return true;
}

template <class V, class T>
bool jmx_import( const jmx::Struct& in, problem_hist<V,T>& out )
{
    JMX_WASSERT_RF( in.has_fields({ "tstart", "tend", "history" }), "[import.hist] Missing field(s)." )

    out.tstart = in.getnum<T>("tstart");
    out.tend = in.getnum<T>("tend");

    out.bsize = in.getnum<uidx_t>("bsize",DISOL_BLOCK_SIZE);
    JMX_WASSERT_RF( out.bsize > 1, "[import.hist] Block-size should be >1." )

    JMX_WASSERT_RF( jmx_import( in.getstruct("history"), out.ts ), 
        "[import.hist] Could not import history." )

    if ( out.tstart != out.ts.tend() ) {
        JMX_WARN("[import.hist] Forcing initial timepoint to match last point in history.")
        out.tstart = out.ts.tend();
    }

    return true;
}

// ------------------------------------------------------------------------

template <class H, class M>
bool jmx_import( const jmx::Struct& in, properties<H,M>& out )
{
    JMX_WASSERT_RF( in.has_fields({ "step", "error" }), "[import.prop] Missing field(s)." )
    return jmx_import(in.getstruct("step"), out.step) 
        && jmx_import(in.getstruct("error"), out.error);
}

template <class T>
bool jmx_import( const jmx::Struct& in, prop_error<T>& out )
{
    out.set_defaults();

    if ( in.has_field("abs") ) out.abs_tol = in.getnum<T>("abs");
    if ( in.has_field("rel") ) out.rel_tol = in.getnum<T>("rel");
    out.norm_control = in.getbool( "norm_control", false );

    return true;
}

template <class T>
bool jmx_import( const jmx::Struct& in, prop_step<T>& out )
{
    if ( in.has_field("nsteps") ) 
    {
        out.num_steps = in.getnum<uidx_t>("nsteps"); 
        out.set_step(0);
    }
    else if ( in.has_field("dt") )
    {
        out.set_step(in.getnum<T>("dt"));
        out.num_steps = 0; // used by integrators
    }
    else if ( in.has_fields({"min","max"}) )
    {
        out.min_step = in.getnum<T>("min");
        out.max_step = in.getnum<T>("max");
        out.init_step = in.getnum<T>("init", (out.min_step+out.max_step)/2 );
        out.num_steps = 0; // used by integrators
    }
    else 
    {
        JMX_WARN("[import.prop.step] Missing field(s).")
        return false;
    }
    return true;
}

// ------------------------------------------------------------------------

template <class V, class T>
bool jmx_import( const jmx::Struct& in, ts_shr<V,T>& out )
{
    using mT = typename ts_shr<V,T>::time_type;
    using mV = typename ts_shr<V,T>::value_type;
    JMX_WASSERT_RF( in.has_fields({"time","vals"}), "[import.ts] Missing field(s)." )

    auto t = _jmx_to_svec(in.getvec<mT>("time"));
    auto d = _jmx_to_smat(in.getvec<mV>("vals"));
    return out.assign(t,d);
}

template <class V, class T>
bool jmx_import( const jmx::Struct& in, tc_shr<V,T>& out )
{
    using mT = typename tc_shr<V,T>::time_type;
    using mV = typename tc_shr<V,T>::value_type;
    JMX_WASSERT_RF( in.has_fields({"time","vals"}), "[import.tc] Missing field(s)." )

    auto t = _jmx_to_svec(in.getvec<mT>("time"));
    auto d = _jmx_to_svec(in.getvec<mV>("vals"));
    return out.assign(t,d);
}

// ------------------------------------------------------------------------

template <class V, class T>
bool jmx_import( const jmx::Struct& in, ts_ptr<V,T>& out )
{
    using mT = typename ts_ptr<V,T>::mtime_type;
    using mV = typename ts_ptr<V,T>::mvalue_type;
    JMX_WASSERT_RF( in.has_fields({"time","vals"}), "[import.ts_ref] Missing field(s)." )

    auto t = _jmx_to_pvec(in.getvec<mT>("time"));
    auto d = _jmx_to_pmat(in.getmat<mV>("vals"));
    return out.assign(t,d);
}

template <class V, class T>
bool jmx_import( const jmx::Struct& in, tc_ptr<V,T>& out )
{
    using mT = typename tc_ptr<V,T>::mtime_type;
    using mV = typename tc_ptr<V,T>::mvalue_type;
    JMX_WASSERT_RF( in.has_fields({"time","vals"}), "[import.tc_ref] Missing field(s)." )

    auto t = _jmx_to_pvec(in.getvec<mT>("time"));
    auto d = _jmx_to_pvec(in.getvec<mV>("vals"));
    return out.assign(t,d);
}

// ------------------------------------------------------------------------

template <class V, class T>
bool jmx_export( const ts_shr<V,T>& in, jmx::Struct out )
{
    using mT = typename ts_shr<V,T>::time_type;
    using mV = typename ts_shr<V,T>::value_type;
    _jmx_to_pvec(out.mkvec<mT>( "time", in.ntime(), true )).copy(in.time());
    _jmx_to_pvec(out.mkmat<mV>( "vals", in.ntime(), in.nchan() )).copy(in.data().data());
    return true;
}

template <class V, class T>
bool jmx_export( const tc_shr<V,T>& in, jmx::Struct out )
{
    using mT = typename tc_shr<V,T>::time_type;
    using mV = typename tc_shr<V,T>::value_type;
    _jmx_to_pvec(out.mkvec<mT>( "time", in.ntime(), true )).copy(in.time());
    _jmx_to_pvec(out.mkvec<mV>( "vals", in.ntime(), true )).copy(in.data());
    return true;
}

// ------------------------------------------------------------------------

template <class V, class T>
bool jmx_export( const ts_ptr<V,T>& in, jmx::Struct out )
{
    using mT = typename ts_ptr<V,T>::mtime_type;
    using mV = typename ts_ptr<V,T>::mvalue_type;
    _jmx_to_pvec(out.mkvec<mT>( "time", in.ntime(), true )).copy(in.time());
    _jmx_to_pvec(out.mkmat<mV>( "vals", in.ntime(), in.nchan() )).copy(in.data().data());
    return true;
}

template <class V, class T>
bool jmx_export( const tc_ptr<V,T>& in, jmx::Struct out )
{
    using mT = typename tc_ptr<V,T>::mtime_type;
    using mV = typename tc_ptr<V,T>::mvalue_type;
    _jmx_to_pvec(out.mkvec<mT>( "time", in.ntime(), true )).copy(in.time());
    _jmx_to_pvec(out.mkvec<mV>( "vals", in.ntime(), true )).copy(in.data());
    return true;
}

// ------------------------------------------------------------------------

template <class V, class T>
bool jmx_export( const dr::timepool<V,T>& in, jmx::Struct out )
{
    const uidx_t nt = in.ntime();
    const uidx_t nc = in.nchan();

    auto t = out.mkvec<T>("time",nt,true);
    auto M = out.mkmat<V>("vals",nt,nc);

    for ( uidx_t i=0; i < nt; i++ ) {
        t[i] = in[i];
        for ( uidx_t j=0; j < nc; j++ )
            M(i,j) = in(i,j);
    }
    return true;
}

// ------------------------------------------------------------------------

template <class V, class T>
ts_ptr<V,T> jmx_make_ts( jmx::Struct out, uidx_t nchan, T tstart, T tend, T step )
{
    static_assert( !std::is_const<V>::value && !std::is_const<T>::value, "Types cannot be const." );
    DRAYN_ASSERT_ERR( dr::op_abs(step) > dr::c_num<T>::eps, "Null step." )
    DRAYN_ASSERT_ERR( (tend-tstart)/step > 0, "Empty timecourse (or bad step sign)." )

    const uidx_t n = dr::op_ufloor( (tend-tstart)/step );

    // create timepoints
    auto t = _jmx_to_pvec(out.mkvec<T>( "time", n, true ));
    auto d = _jmx_to_pmat(out.mkmat<V>( "vals", n, nchan ));
    for ( uidx_t i=0; i < n; i++ ) t[i] = fma(i,step,tstart);

    return ts_ptr<V,T>(t,d,true);
}

template <class V, class T>
tc_ptr<V,T> jmx_make_tc( jmx::Struct out, T tstart, T tend, T step )
{
    static_assert( !std::is_const<V>::value && !std::is_const<T>::value, "Types cannot be const." );
    DRAYN_ASSERT_ERR( dr::op_abs(step) > dr::c_num<T>::eps, "Null step." )
    DRAYN_ASSERT_ERR( (tend-tstart)/step > 0, "Empty timecourse (or bad step sign)." )

    const uidx_t n = dr::op_ufloor( (tend-tstart)/step );

    // create timepoints
    auto t = _jmx_to_pvec(out.mkvec<T>( "time", n, true ));
    auto d = _jmx_to_pvec(out.mkvec<V>( "vals", n, true ));
    for ( uidx_t i=0; i < n; i++ ) t[i] = fma(i,step,tstart);

    return tc_ptr<V,T>(t,d,true);
}
