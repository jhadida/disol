
//==================================================
// @title        adaptive_step.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <string>
#include <sstream>



        /********************     **********     ********************/
        /********************     **********     ********************/



DISOL_NS_START

template <class S, class C = controller_default>
class integrator_adaptive_step: public integrator<S>
{
public:

    using self   = integrator_adaptive_step<S,C>;
    using parent = integrator<S>;

    using stepper_type     = S;
    using controller_type  = C;
    using system_type      = typename parent::system_type;
    using problem_type     = typename parent::problem_type;
    using property_type    = typename parent::property_type;
    using handle_type      = typename parent::handle_type;

    static_assert( stepper_type::estimates_error::value,
        "This integrator requires error estimations for step-size adaptation." );

    controller_type controller;

    // ----------  =====  ----------

    integrator_adaptive_step()
        : controller( stepper_type::order ) {}

    inline bool is_adaptive() const { return true; }

    void integrate( system_type& sys, problem_type& pb, property_type& prop )
    {
        // Used to warn about time-step over/under-flow
        // std::stringstream xflow_stream;

        // Initialize and start
        prop.step.num_steps = 0;
        DRAYN_ASSERT_R( this->init( sys, pb, prop ), "Initialisation failed." );
        prop.event.after_init.publish( m_handle );

        while ( ! m_handle.done() )
        {
            this->fetch_next();

            // Make sure time-step is within allowed bounds
            // if( m_handle.cur.dt > prop.step.max_step || m_handle.cur.dt < prop.step.min_step )    
            //     xflow_stream << m_handle.cur.index << ", ";
            m_handle.cur.dt = dr::op_clamp( m_handle.cur.dt, prop.step.min_step, prop.step.max_step );

            // Adjust the timestep to finish exactly at t1
            m_handle.adjust_step();

            // Try a step and update the current time-step,
            // as long as the error doesn't meet the accuracy requirements.
            while ( true )
            {
                prop.event.before_step.publish( m_handle );
                m_handle.err = this->stepper.step( m_handle, prop );

                if ( controller.accept(m_handle.err) ) // step accepted, update next time-step
                {
                    m_handle.next.dt = m_handle.cur.dt * controller.scaling_factor( m_handle.err );
                    m_handle.next.t  = m_handle.cur.t + m_handle.cur.dt;

                    controller.update( m_handle.err ); break;
                }
                else // step rejected, update current time-step
                {
                    controller.update( m_handle.err );
                    m_handle.cur.dt = m_handle.cur.dt * controller.scaling_factor( m_handle.err );
                }
            }

            // Update time-properties
            ++prop.step.num_steps;
            prop.event.after_step.publish( m_handle );

            this->commit_next();
            prop.event.after_commit.publish( m_handle );
        }

        // // uncomment this to show indices
        // if ( xflow_stream.tellp() )
        // {
        //     xflow_stream << "\b\b "; // remove last separator
        //     DRAYN_INFO( "The following timepoint(s) (numbered from 0) correspond to time-step over/under-flow:\n%s", xflow_stream.str().c_str() );
        // }
    }


protected:
    using parent::m_handle;
};

DISOL_NS_END
