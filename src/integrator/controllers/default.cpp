
//==================================================
// @title        default.cpp
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



DISOL_NS_START

bool controller_default::update( double err )
{
    if ( this->accept(err) ) {
        // dr_println("Error: %g",err);
        reject_count = 0;
        return true;
    }
    else if ( max_fail && (++reject_count > max_fail) )
        throw std::runtime_error("Maximum number of reject exceeded.");

    return false;
}

// ------------------------------------------------------------------------

double controller_default::scaling_factor( double err )
{
    bool   acc = this->accept(err);
    double lmx = ( !acc || reject_count ) ? 1.0 : max_scale; // no increase if reject
    // double fac = acc ? std::pow(err_old,beta) : 1.0; // no PI if reject
    double fac = (reject_count) ? 0.5 : safe_scale;

    // update error
    if (acc) err_old = std::max( err, min_error );

    return ( err < dr::c_num<double>::eps ) ?
        lmx : dr::op_clamp( std::pow(err,-alpha)*fac, min_scale, lmx );

    // return ( err < dr::c_num<double>::eps ) ?
    //  lmx : dr::op_clamp( safe_scale*std::pow(err,-alpha)*fac, min_scale, lmx );
}

DISOL_NS_END
