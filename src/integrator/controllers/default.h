
//==================================================
// @title        default.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <cmath>
#include <cstdint>
#include <algorithm>
#include <stdexcept>



        /********************     **********     ********************/
        /********************     **********     ********************/



DISOL_NS_START

/**
 * dt_{n+1} / dt_n = safe_scale * pow(err,-alpha) * pow(err,beta)
 *
 * Recommended values:
 *  beta = 0.4/k, alpha = 0.7/k
 * where k is the order of the stepper.
 */
struct controller_default: public controller
{
    // change before integration to modify behaviour
    double min_error, max_fail; 
    
    double alpha, beta;
    double min_scale, max_scale, safe_scale;
    double err_old;

    uint64_t reject_count;

    // ----------  =====  ----------

    controller_default( unsigned order = 1 )
    {
        if ( order == 0 ) {
            DRAYN_WARN( "Order cannot be 0, setting to 1 instead." );
            order = 1;
        }

        beta  = 0.0/order;
        alpha = 1.0/order - 0.75*beta;

        safe_scale = 0.80;
        min_scale  = 0.10;
        max_scale  = 5.00;

        min_error = 1e-4;
        max_fail = 25;

        err_old      = min_error;
        reject_count = 0;
    }

    bool   update         ( double err );
    double scaling_factor ( double err );
};

DISOL_NS_END
