
//==================================================
// @title        ross.cpp
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



DISOL_NS_START

bool controller_ross::update( double err )
{
    if ( this->accept(err) )
    {
        reject_count = 0;
        return true;
    }
    else if ( max_fail && (++reject_count > max_fail) )
        throw std::runtime_error("Maximum number of reject exceeded.");

    return false;
}

// ------------------------------------------------------------------------

double controller_ross::scaling_factor( double err )
{
    bool   acc = this->accept(err);
    double lmx = ( !acc || reject_count ) ? 1.0 : max_scale; // no increase if reject
    double fac = ( acc && !first_step && err*err_old >= dr::c_num<double>::eps ) ?
        std::min( scale_old*std::pow(err/err_old,-alpha), 1.0 ) : 1.0;

    double scale = ( err < dr::c_num<double>::eps ) ?
        lmx : dr::op_clamp( safe_scale*std::pow(err,-alpha)*fac, min_scale, lmx );

    // update error and scale
    if (acc)
    {
        err_old    = std::max( err, min_error );
        first_step = false;
        scale_old  = scale;
    }

    return scale;
}

DISOL_NS_END
