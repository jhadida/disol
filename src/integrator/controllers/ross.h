
//==================================================
// @title        ross.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <cmath>
#include <cstdint>
#include <algorithm>
#include <stdexcept>



        /********************     **********     ********************/
        /********************     **********     ********************/



DISOL_NS_START

struct controller_ross: public controller
{
    // change before integration to modify behaviour
    double min_error, max_fail; 

    double alpha, err_old, scale_old;
    double min_scale, max_scale, safe_scale;

    uint64_t reject_count;
    bool first_step;

    // ----------  =====  ----------

    controller_ross( unsigned order = 4 )
    {
        if ( order == 0 ) {
            DRAYN_WARN( "Order cannot be 0, setting to 1 instead." );
            order = 1;
        }

        alpha = 1.0/order;

        safe_scale = 0.80; // 0.95
        min_scale  = 0.10; // 0.2
        max_scale  = 5.00;

        min_error = 1e-2;
        max_fail = 25;

        err_old      = min_error;
        scale_old    = 1;
        reject_count = 0;
        first_step   = true;
    }

    bool   update         ( double err );
    double scaling_factor ( double err );
};

DISOL_NS_END
