
//==================================================
// @title        fixed_length.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



DISOL_NS_START

template <class S>
class integrator_fixed_length: public integrator<S>
{
public:

    using self   = integrator_fixed_length<S>;
    using parent = integrator<S>;

    using stepper_type   = S;
    using system_type    = typename parent::system_type;
    using problem_type   = typename parent::problem_type;
    using property_type  = typename parent::property_type;

    // ----------  =====  ----------

    inline bool is_adaptive() const { return false; }

    void integrate( system_type& sys, problem_type& pb, property_type& prop )
    {
        // Compute timestep given time-interval and number of steps
        DRAYN_ASSERT_R( prop.step.num_steps > 1, "Number of steps must be at least 2." );

        prop.step.init_step = prop.step.min_step = prop.step.max_step =
            pb.tspan() / (prop.step.num_steps - 1);
        prop.step.num_steps = 0;

        // Initialise
        DRAYN_ASSERT_R( this->init( sys, pb, prop ), "Initialisation failed." );
        prop.event.after_init.publish( m_handle );

        while ( ! m_handle.done() )
        {
            this->fetch_next();

            // Here we could adjust cur.dt to finish exactly at t1

            prop.event.before_step.publish( m_handle );
            m_handle.err = this->stepper.step( m_handle, prop );

            // Here we could adjust the next timestep using the error

            // update time-properties
            ++prop.step.num_steps;
            m_handle.next.t  = m_handle.tstart() + prop.step.num_steps * m_handle.cur.dt;
            m_handle.next.dt = m_handle.cur.dt;

            // nothing to do before publishing this event for fixed-length integration
            prop.event.after_step.publish( m_handle );

            this->commit_next();
            prop.event.after_commit.publish( m_handle );
        }
    }

protected:
    using parent::m_handle;
};

DISOL_NS_END
