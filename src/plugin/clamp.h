
//==================================================
// @title        clamp.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

/**
 * Example plugin to enforce range-condition on states during integration.
 * Can be configured from an input Matlab structure.
 */


        /********************     **********     ********************/
        /********************     **********     ********************/



DISOL_NS_START

template <class H>
struct plugin_clamp: public plugin<H>
{
public:

    using parent      = plugin<H>;
    using handle_type = H;

    using signal_type = typename parent::signal_type;
    using slot_type   = typename parent::slot_type;
    using time_type   = typename handle_type::time_type;
    using value_type  = typename handle_type::value_type;

    // ----------  =====  ----------

    value_type lower, upper;

    plugin_clamp()
        { configure(0,1); }
    plugin_clamp( signal_type& sig )
        { configure(0,1); this->attach(sig); }

    bool configure( value_type l, value_type u ) {
        lower = l; upper = u;
        return true;
    }

protected:

    virtual void callback( handle_type& h ) {
        for ( auto it = dr::iterate(h.next.x); it; it.next() )
            *it = dr::op_clamp( *it, lower, upper );
    }
};

DISOL_NS_END
