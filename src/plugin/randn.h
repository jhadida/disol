
//==================================================
// @title        randn.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

/**
 * Another example to add some noise on the states during integration.
 * Both sigma and mu can be configured.
 *
 * Note that the proper way of "adding" noise to differential equations
 * is to formulate them as an Ito system and use a stochastic stepper,
 * for example disol::stepper_SRK2.
 */


        /********************     **********     ********************/
        /********************     **********     ********************/



DISOL_NS_START

template <class H>
struct plugin_randn
    : public plugin<H>
{
public:

    using parent      = plugin<H>;
    using handle_type = H;
    using signal_type = typename parent::signal_type;
    using slot_type   = typename parent::slot_type;
    using time_type   = typename handle_type::time_type;
    using value_type  = typename handle_type::value_type;

    using noise_dist  = std::normal_distribution<value_type>;
    using noise_gen   = dr::generator_type<noise_dist>;

    noise_gen noise; // the noise generator

    // ----------  =====  ----------

    plugin_randn()
        { configure(1); }
    plugin_randn( signal_type& sig )
        { configure(1); this->attach(sig); }

    bool configure( value_type sigma, value_type mu = 0 ) {
        noise = dr::normal_gen<value_type>( mu, sigma );
        return true;
    }

protected:

    virtual void callback( handle_type& h ) {
        for ( auto it = dr::iterate(h.next.x); it; it.next() )
            *it += noise();
    }
};

DISOL_NS_END
