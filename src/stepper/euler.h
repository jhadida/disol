
//==================================================
// @title        euler.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



DISOL_NS_START

//-------------------------------------------------------------
// Forward Euler method
//-------------------------------------------------------------

template <class S>
struct stepper_Euler: public stepper<S,1>
{
    using parent         = stepper<S,1>;
    using system_type    = S;
    using time_type      = typename parent::time_type;
    using value_type     = typename parent::value_type;
    using property_type  = typename parent::property_type;
    using handle_type    = typename parent::handle_type;

    // ----------  =====  ----------

    double step( handle_type& h, property_type& prop )
    {
        DRAYN_DASSERT_RV( h.valid(), 0.0, "Invalid handle.");

        h.sys->derivative( h.cur.t, h.cur.x, h.cur.dxdt );

        h.next.t = h.cur.t + h.cur.dt;
        algebra::iter_apply_3( h.next.x, h.cur.x, h.cur.dxdt,
            algebra::weighted_sum_2<double>( 1.0, h.cur.dt ) );

        return 0.0;
    }
};

DISOL_NS_END
