
//==================================================
// @title        rk2.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



DISOL_NS_START

//-------------------------------------------------------------
// Runge-Kutta 2, aka midpoint method.
//-------------------------------------------------------------

template <class S>
struct stepper_RK2: public stepper<S,2>
{
    using parent         = stepper<S,2>;
    using system_type    = S;
    using time_type      = typename parent::time_type;
    using value_type     = typename parent::value_type;
    using property_type  = typename parent::property_type;
    using handle_type    = typename parent::handle_type;

    // ----------  =====  ----------

    inline double smallest_substep() const { return 0.5; }

    double step( handle_type& h, property_type& prop )
    {
        DRAYN_DASSERT_RV( h.valid(), 0.0, "Invalid handle.");

        auto& sys = *h.sys;

        // Set cur.dxdt
        sys.derivative( h.cur.t, h.cur.x, h.cur.dxdt );

        // Half an Euler step
        algebra::iter_apply_3( h.next.x, h.cur.x, h.cur.dxdt,
            algebra::weighted_sum_2<double>( 1.0, h.cur.dt/2 ) );

        // Compute dxdt there
        h.next.t = h.cur.t + h.cur.dt/2;
        sys.derivative( h.next.t, h.next.x, h.cur.dxdt );

        // Use this new estimate for an Euler step
        h.next.t = h.cur.t + h.cur.dt;
        algebra::iter_apply_3( h.next.x, h.cur.x, h.cur.dxdt,
            algebra::weighted_sum_2<double>( 1.0, h.cur.dt ) );

        return 0.0;
    }
};

DISOL_NS_END
