
//==================================================
// @title        rk4.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



DISOL_NS_START

template <class S>
struct stepper_RK4: public stepper<S,4>
{
    using parent         = stepper<S,4>;
    using system_type    = S;
    using time_type      = typename parent::time_type;
    using value_type     = typename parent::value_type;
    using property_type  = typename parent::property_type;
    using handle_type    = typename parent::handle_type;

    // ----------  =====  ----------

    inline double smallest_substep() const { return 0.5; }

    stepper_RK4() { clear(); }

    void clear() 
    { 
        k1.clear();
        k2.clear();
        k3.clear();
        k4.clear();
        kdata.clear(); 
    }

    bool init( handle_type& h, property_type& prop )
        { return parent::init(h,prop) && resize(h.size()); }

    double step ( handle_type& h, property_type& prop )
    {
        DRAYN_ASSERT_RV( h.valid(), 0.0, "Invalid handle.");

        // Prepare k1 through k4 arrays
        auto& sys = *h.sys;
        auto dt = h.cur.dt;
        auto x = h.next.x;

        // K1
        sys.derivative( h.cur.t, h.cur.x, k1 );

        // K2
        algebra::iter_apply_3( x, h.cur.x, k1, 
            algebra::weighted_sum_2<double>( 1.0, dt/2 ) );
        sys.derivative( h.cur.t + dt/2, x, k2 );

        // K3
        algebra::iter_apply_3( x, h.cur.x, k2, 
            algebra::weighted_sum_2<double>( 1.0, dt/2 ) );
        sys.derivative( h.cur.t + dt/2, x, k3 );

        // K4
        algebra::iter_apply_3( x, h.cur.x, k3, 
            algebra::weighted_sum_2<double>( 1.0, dt ) );
        sys.derivative( h.cur.t + dt, x, k4 );

        // Final estimate
        h.next.t = h.cur.t + dt;
        algebra::iter_apply_6( h.next.x, h.cur.x, k1, k2, k3, k4,
            algebra::weighted_sum_5<double>( 1.0, 1/6.0, 1/3.0, 1/3.0, 1/6.0, dt ) );

        return 0.0;
    }

private:

    bool resize( uidx_t n )
    {
        if ( kdata.size() != 4*n )
        {
            kdata.resize( 4*n );

            k1.assign( &kdata[0*n], n );
            k2.assign( &kdata[1*n], n );
            k3.assign( &kdata[2*n], n );
            k4.assign( &kdata[3*n], n );
        }
        return true;
    }

    svec<value_type> kdata;
    pvec<value_type> k1, k2, k3, k4;
};

DISOL_NS_END
