
//==================================================
// @title        rk45_dopri.cpp
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



DISOL_NS_START

// k2 = f(k1)
const double rk45_dopri_constants::w21 = 1.0 / 5.0;
const double rk45_dopri_constants::t2  = w21;

// k3 = f(k1,k2)
const double rk45_dopri_constants::w31 = 3.0 / 40.0;
const double rk45_dopri_constants::w32 = 9.0 / 40.0;
const double rk45_dopri_constants::t3  = 3.0 / 10.0; // sum w3i

// k4 = f(k1,k2,k3)
const double rk45_dopri_constants::w41 = 44.0 / 45.0;
const double rk45_dopri_constants::w42 = -56.0 / 15.0;
const double rk45_dopri_constants::w43 = 32.0 / 9.0;
const double rk45_dopri_constants::t4  = 4.0 / 5.0; // sum w4i

// k5 = f(k1,k2,k3,k4)
const double rk45_dopri_constants::w51 = 19372.0 / 6561.0;
const double rk45_dopri_constants::w52 = -25360.0 / 2187.0;
const double rk45_dopri_constants::w53 = 64448.0 / 6561.0;
const double rk45_dopri_constants::w54 = -212.0 / 729.0;
const double rk45_dopri_constants::t5  = 8.0 / 9.0; // sum w5i

// k6 = f(k1,k2,k3,k4,k5)
const double rk45_dopri_constants::w61 = 9017.0 / 3168.0;
const double rk45_dopri_constants::w62 = -355.0 / 33.0;
const double rk45_dopri_constants::w63 = 46732.0 / 5247.0;
const double rk45_dopri_constants::w64 = 49.0 / 176.0;
const double rk45_dopri_constants::w65 = -5103.0 / 18656.0;

// Output = f(k1,k3,k4,k5,k6)
const double rk45_dopri_constants::a1 = 35.0 / 384.0;
const double rk45_dopri_constants::a3 = 500.0 / 1113.0;
const double rk45_dopri_constants::a4 = 125.0 / 192.0;
const double rk45_dopri_constants::a5 = -2187.0 / 6784.0;
const double rk45_dopri_constants::a6 = 11.0 / 84.0;

// Estimate = f(k1,k3,k4,k5,k6,k7)
const double rk45_dopri_constants::b1 = 5179.0 / 57600.0;
const double rk45_dopri_constants::b3 = 7571.0 / 16695.0;
const double rk45_dopri_constants::b4 = 393.0 / 640.0;
const double rk45_dopri_constants::b5 = -92097.0 / 339200.0;
const double rk45_dopri_constants::b6 = 187.0 / 2100.0;
const double rk45_dopri_constants::b7 = 1.0 / 40.0;

const double rk45_dopri_constants::e1 = 71.0 / 57600.0;
const double rk45_dopri_constants::e3 = -71.0 / 16695.0;
const double rk45_dopri_constants::e4 = 71.0 / 1920.0;
const double rk45_dopri_constants::e5 = -17253.0 / 339200.0;
const double rk45_dopri_constants::e6 = 22.0 / 525.0;
const double rk45_dopri_constants::e7 = -1.0 / 40.0;

const double rk45_dopri_constants::d1 = -12715105075.0 / 11282082432.0;
const double rk45_dopri_constants::d3 = 87487479700.0 / 32700410799.0;
const double rk45_dopri_constants::d4 = -10690763975.0 / 1880347072.0;
const double rk45_dopri_constants::d5 = 701980252875.0 / 199316789632.0;
const double rk45_dopri_constants::d6 = -1453857185.0 / 822651844.0;
const double rk45_dopri_constants::d7 = 69997945.0 / 29380423.0;

DISOL_NS_END
