
//==================================================
// @title        rk45_dopri.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <cmath>

// Base on:
// http://www.unige.ch/~hairer/prog/nonstiff/dopri5.f



        /********************     **********     ********************/
        /********************     **********     ********************/



DISOL_NS_START

// Constants for the Dormand-Prince 45 method.
struct rk45_dopri_constants {
    static const double
    t2, t3, t4, t5, t6,
    a1, a3, a4, a5, a6,
    b1, b3, b4, b5, b6, b7,
    e1, e3, e4, e5, e6, e7,
    d1, d3, d4, d5, d6, d7,
    w21,
    w31, w32,
    w41, w42, w43,
    w51, w52, w53, w54,
    w61, w62, w63, w64, w65;
};

// ------------------------------------------------------------------------

template <class S>
struct stepper_RK45_Dopri
    : public stepper<S,5,true>, private rk45_dopri_constants
{
    using parent         = stepper<S,5,true>;
    using system_type    = S;
    using metric_type    = typename parent::metric_type;
    using time_type      = typename parent::time_type;
    using value_type     = typename parent::value_type;
    using property_type  = typename parent::property_type;
    using handle_type    = typename parent::handle_type;

    // ----------  =====  ----------

    inline double smallest_substep() const { return t2; }

    stepper_RK45_Dopri() { clear(); }

    void clear()
    {
        y.clear();
        k1.clear();
        k2.clear();
        k3.clear();
        k4.clear();
        k5.clear();
        k6.clear();
        k7.clear();
        kdata.clear();
    }

    bool init( handle_type& h, property_type& prop )
        { return parent::init(h,prop) && resize(h.size()); }

    double step( handle_type& h, property_type& prop )
    {
        DRAYN_ASSERT_RV( h.valid(), 0.0, "Invalid handle.");
        const uidx_t nd = h.cur.size();


        // ----------  =====  ----------
        // Part 1: Evaluate K-points

        // Prepare k1 through k6 arrays
        auto& sys = *h.sys;
        auto dt = h.cur.dt;
        auto x = h.next.x;

        // K1
        sys.derivative( h.cur.t, h.cur.x, k1 );

        // K2
        algebra::iter_apply_3( x, h.cur.x, k1,
            algebra::weighted_sum_2<double>( 1.0, dt*w21 ) );
        sys.derivative( h.cur.t + dt*t2, x, k2 );

        // K3
        algebra::iter_apply_4( x, h.cur.x, k1, k2,
            algebra::weighted_sum_3<double>( 1.0, w31, w32, dt ) );
        sys.derivative( h.cur.t + dt*t3, x, k3 );

        // K4
        algebra::iter_apply_5( x, h.cur.x, k1, k2, k3,
            algebra::weighted_sum_4<double>( 1.0, w41, w42, w43, dt ) );
        sys.derivative( h.cur.t + dt*t4, x, k4 );

        // K5
        algebra::iter_apply_6( x, h.cur.x, k1, k2, k3, k4,
            algebra::weighted_sum_5<double>( 1.0, w51, w52, w53, w54, dt ) );
        sys.derivative( h.cur.t + dt*t5, x, k5 );

        // K6
        algebra::iter_apply_7( x, h.cur.x, k1, k2, k3, k4, k5,
            algebra::weighted_sum_6<double>( 1.0, w61, w62, w63, w64, w65, dt ) );
        sys.derivative( h.cur.t + dt, x, k6 );


        // ----------  =====  ----------
        // Part 2: Final estimates
        h.next.t = h.cur.t + dt;

        algebra::iter_apply_7( h.next.x, h.cur.x, k1, k3, k4, k5, k6,
            algebra::weighted_sum_6<double>( 1.0, a1, a3, a4, a5, a6, dt ) );
        sys.derivative( h.cur.t + dt, h.next.x, k7 );

        algebra::iter_apply_7( y, k1, k3, k4, k5, k6, k7,
            algebra::weighted_sum_6<double>( dt*e1, e3, e4, e5, e6, e7, dt ) );


        // ----------  =====  ----------
        // Part 3: Error estimate

        double error, scale, delta;
        if ( prop.error.norm_control )
        {
            scale = std::max( prop.metric.norm(h.cur.x), prop.metric.norm(h.next.x) );
            scale = std::max( prop.error.abs_tol, prop.error.rel_tol * scale );
            // scale = prop.error.abs_tol + prop.error.rel_tol * scale;
            error = prop.metric.norm(y) / scale;
            // error = error / sqrt(nd);
        }
        else
        {
            error = 0.0;
            for ( uidx_t i = 0; i < nd; ++i )
            {
                scale = std::max( dr::op_abs(h.cur.x[i]), dr::op_abs(h.next.x[i]) );
                scale = std::max( prop.error.abs_tol, prop.error.rel_tol * scale );
                // scale = prop.error.abs_tol + prop.error.rel_tol*scale;

                delta = y[i]/scale;
                error += delta*delta;
            }
            error = sqrt( error/nd );
            // error = sqrt( error );
        }

        return error;
    }

private:

    bool resize( uidx_t n )
    {
        if ( kdata.size() != 8*n )
        {
            kdata.resize(8*n);

            k1.assign( &kdata[0*n], n );
            k2.assign( &kdata[1*n], n );
            k3.assign( &kdata[2*n], n );
            k4.assign( &kdata[3*n], n );
            k5.assign( &kdata[4*n], n );
            k6.assign( &kdata[5*n], n );
            k7.assign( &kdata[6*n], n );

            y.assign( &kdata[7*n], n );
        }
        return true;
    }

    svec<value_type> kdata;
    pvec<value_type> k1, k2, k3, k4, k5, k6, k7, y;
};

DISOL_NS_END
