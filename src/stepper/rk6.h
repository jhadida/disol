
//==================================================
// @title        rk6.cpp
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

// Based on:
// http://www.ams.org/journals/mcom/1968-22-102/S0025-5718-68-99876-1/S0025-5718-68-99876-1.pdf



        /********************     **********     ********************/
        /********************     **********     ********************/



DISOL_NS_START

struct rk6_constants {
    static const double
    a1, a3, a4, a5, a6, a7,
    t2, t3, t4, t5, t6,
    w21,
    w31, w32,
    w41, w42, w43,
    w51, w52, w53, w54,
    w61, w62, w63, w64, w65,
    w71, w72, w73, w74, w75, w76;
};

// ------------------------------------------------------------------------

template <class S>
struct stepper_RK6
    : public stepper<S,6>, private rk6_constants
{
    using parent        = stepper<S,6>;
    using system_type   = S;
    using time_type     = typename parent::time_type;
    using value_type    = typename parent::value_type;
    using property_type = typename parent::property_type;
    using handle_type   = typename parent::handle_type;

    // ----------  =====  ----------

    inline double smallest_substep() const { return t5; }

    stepper_RK6() { clear(); }

    void clear()
    {
        k1.clear();
        k2.clear();
        k3.clear();
        k4.clear();
        k5.clear();
        k6.clear();
        kdata.clear();
    }

    bool init( handle_type& h, property_type& prop )
        { return parent::init(h,prop) && resize(h.size()); }

    double step ( handle_type& h, property_type& prop )
    {
        DRAYN_ASSERT_RV( h.valid(), 0.0, "Invalid handle.");

        // Prepare k1 through k4 arrays
        auto& sys = *h.sys;
        auto dt = h.cur.dt;
        auto x = h.next.x;

        // K1
        sys.derivative( h.cur.t, h.cur.x, k1 );

        // K2
        algebra::iter_apply_3( x, h.cur.x, k1,
            algebra::weighted_sum_2<double>( 1.0, dt*w21 ) );
        sys.derivative( h.cur.t + dt*t2, x, k2 );

        // K3
        algebra::iter_apply_4( x, h.cur.x, k1, k2,
            algebra::weighted_sum_3<double>( 1.0, w31, w32, dt ) );
        sys.derivative( h.cur.t + dt*t3, x, k3 );

        // K4
        algebra::iter_apply_5( x, h.cur.x, k1, k2, k3,
            algebra::weighted_sum_4<double>( 1.0, w41, w42, w43, dt ) );
        sys.derivative( h.cur.t + dt*t4, x, k4 );

        // K5
        algebra::iter_apply_6( x, h.cur.x, k1, k2, k3, k4,
            algebra::weighted_sum_5<double>( 1.0, w51, w52, w53, w54, dt ) );
        sys.derivative( h.cur.t + dt*t5, x, k5 );

        // K6
        algebra::iter_apply_7( x, h.cur.x, k1, k2, k3, k4, k5,
            algebra::weighted_sum_6<double>( 1.0, w61, w62, w63, w64, w65, dt ) );
        sys.derivative( h.cur.t + dt*t6, x, k6 );

        // K7
        algebra::iter_apply_8( x, h.cur.x, k1, k2, k3, k4, k5, k6,
            algebra::weighted_sum_7<double>( 1.0, w71, w72, w73, w74, w75, w76, dt ) );
        sys.derivative( h.cur.t + dt, x, k2 );

        // Final estimate
        h.next.t = h.cur.t + dt;
        algebra::iter_apply_7( h.next.x, h.cur.x, k1, k3, k5, k6, k2,
            algebra::weighted_sum_6<double>( 1.0, a1, a3, a5, a6, a7, dt ) );

        return 0.0;
    }

private:

    bool resize( uidx_t n )
    {
        if ( kdata.size() != 6*n )
        {
            kdata.resize( 6*n );

            k1.assign( &kdata[0*n], n );
            k2.assign( &kdata[1*n], n );
            k3.assign( &kdata[2*n], n );
            k4.assign( &kdata[3*n], n );
            k5.assign( &kdata[4*n], n );
            k6.assign( &kdata[5*n], n );
        }
        return true;
    }

    svec<value_type> kdata;
    pvec<value_type> k1, k2, k3, k4, k5, k6;
};

DISOL_NS_END
