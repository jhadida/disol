
//==================================================
// @title        rk853_dopri.cpp
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



DISOL_NS_START

const double rk853_dopri_constants::t2  = 0.526001519587677318785587544488e-1;
const double rk853_dopri_constants::t3  = 0.789002279381515978178381316732e-1;
const double rk853_dopri_constants::t4  = 0.118350341907227396726757197510;
const double rk853_dopri_constants::t5  = 0.281649658092772603273242802490;
const double rk853_dopri_constants::t6  = 0.333333333333333333333333333333;
const double rk853_dopri_constants::t7  = 0.25;
const double rk853_dopri_constants::t8  = 0.307692307692307692307692307692;
const double rk853_dopri_constants::t9  = 0.651282051282051282051282051282;
const double rk853_dopri_constants::t10 = 0.6;
const double rk853_dopri_constants::t11 = 0.857142857142857142857142857142;

const double rk853_dopri_constants::a1  =  2.27331014751653820792359768449;
const double rk853_dopri_constants::a4  = -1.05344954667372501984066689879e1;
const double rk853_dopri_constants::a5  = -2.00087205822486249909675718444;
const double rk853_dopri_constants::a6  = -1.79589318631187989172765950534e1;
const double rk853_dopri_constants::a7  =  2.79488845294199600508499808837e1;
const double rk853_dopri_constants::a8  = -2.85899827713502369474065508674;
const double rk853_dopri_constants::a9  = -8.87285693353062954433549289258;
const double rk853_dopri_constants::a10 =  1.23605671757943030647266201528e1;
const double rk853_dopri_constants::a11 =  6.43392746015763530355970484046e-1;

const double rk853_dopri_constants::b1  =  5.42937341165687622380535766363e-2;
const double rk853_dopri_constants::b6  =  4.45031289275240888144113950566;
const double rk853_dopri_constants::b7  =  1.89151789931450038304281599044;
const double rk853_dopri_constants::b8  = -5.8012039600105847814672114227;
const double rk853_dopri_constants::b9  =  3.1116436695781989440891606237e-1;
const double rk853_dopri_constants::b10 = -1.52160949662516078556178806805e-1;
const double rk853_dopri_constants::b11 =  2.01365400804030348374776537501e-1;
const double rk853_dopri_constants::b12 =  4.47106157277725905176885569043e-2;

// ----------  =====  ----------

const double rk853_dopri_constants::e11  = 0.244094488188976377952755905512;
const double rk853_dopri_constants::e19  = 0.733846688281611857341361741547;
const double rk853_dopri_constants::e112 = 0.220588235294117647058823529412e-1;

const double rk853_dopri_constants::e21  =  0.1312004499419488073250102996e-1;
const double rk853_dopri_constants::e26  = -0.1225156446376204440720569753e1;
const double rk853_dopri_constants::e27  = -0.4957589496572501915214079952;
const double rk853_dopri_constants::e28  =  0.1664377182454986536961530415e1;
const double rk853_dopri_constants::e29  = -0.3503288487499736816886487290;
const double rk853_dopri_constants::e210 =  0.3341791187130174790297318841;
const double rk853_dopri_constants::e211 =  0.8192320648511571246570742613e-1;
const double rk853_dopri_constants::e212 = -0.2235530786388629525884427845e-1;

// ----------  =====  ----------

const double rk853_dopri_constants::w21 = 5.26001519587677318785587544488e-2;

const double rk853_dopri_constants::w31 = 1.97250569845378994544595329183e-2;
const double rk853_dopri_constants::w32 = 5.91751709536136983633785987549e-2;

const double rk853_dopri_constants::w41 = 2.95875854768068491816892993775e-2;
const double rk853_dopri_constants::w43 = 8.87627564304205475450678981324e-2;

const double rk853_dopri_constants::w51 =  2.41365134159266685502369798665e-1;
const double rk853_dopri_constants::w53 = -8.84549479328286085344864962717e-1;
const double rk853_dopri_constants::w54 =  9.24834003261792003115737966543e-1;

const double rk853_dopri_constants::w61 = 3.7037037037037037037037037037e-2;
const double rk853_dopri_constants::w64 = 1.70828608729473871279604482173e-1;
const double rk853_dopri_constants::w65 = 1.25467687566822425016691814123e-1;

const double rk853_dopri_constants::w71 =  3.7109375e-2;
const double rk853_dopri_constants::w74 =  1.70252211019544039314978060272e-1;
const double rk853_dopri_constants::w75 =  6.02165389804559606850219397283e-2;
const double rk853_dopri_constants::w76 = -1.7578125e-2;

const double rk853_dopri_constants::w81 =  3.70920001185047927108779319836e-2;
const double rk853_dopri_constants::w84 =  1.70383925712239993810214054705e-1;
const double rk853_dopri_constants::w85 =  1.07262030446373284651809199168e-1;
const double rk853_dopri_constants::w86 = -1.53194377486244017527936158236e-2;
const double rk853_dopri_constants::w87 =  8.27378916381402288758473766002e-3;

const double rk853_dopri_constants::w91 =  6.24110958716075717114429577812e-1;
const double rk853_dopri_constants::w94 = -3.36089262944694129406857109825;
const double rk853_dopri_constants::w95 = -8.68219346841726006818189891453e-1;
const double rk853_dopri_constants::w96 =  2.75920996994467083049415600797e1;
const double rk853_dopri_constants::w97 =  2.01540675504778934086186788979e1;
const double rk853_dopri_constants::w98 = -4.34898841810699588477366255144e1;

const double rk853_dopri_constants::w101 =  4.77662536438264365890433908527e-1;
const double rk853_dopri_constants::w104 = -2.48811461997166764192642586468;
const double rk853_dopri_constants::w105 = -5.90290826836842996371446475743e-1;
const double rk853_dopri_constants::w106 =  2.12300514481811942347288949897e1;
const double rk853_dopri_constants::w107 =  1.52792336328824235832596922938e1;
const double rk853_dopri_constants::w108 = -3.32882109689848629194453265587e1;
const double rk853_dopri_constants::w109 = -2.03312017085086261358222928593e-2;

const double rk853_dopri_constants::w111  = -9.3714243008598732571704021658e-1;
const double rk853_dopri_constants::w114  =  5.18637242884406370830023853209;
const double rk853_dopri_constants::w115  =  1.09143734899672957818500254654;
const double rk853_dopri_constants::w116  = -8.14978701074692612513997267357;
const double rk853_dopri_constants::w117  = -1.85200656599969598641566180701e1;
const double rk853_dopri_constants::w118  =  2.27394870993505042818970056734e1;
const double rk853_dopri_constants::w119  =  2.49360555267965238987089396762;
const double rk853_dopri_constants::w1110 = -3.0467644718982195003823669022;

DISOL_NS_END
