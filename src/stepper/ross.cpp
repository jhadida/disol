
//==================================================
// @title        ross.cpp
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



DISOL_NS_START

const double Ross_constants::gam = 0.25;

const double Ross_constants::t2 = 0.386;
const double Ross_constants::t3 = 0.21;
const double Ross_constants::t4 = 0.63;

const double Ross_constants::d1 =  0.2500000000000000;
const double Ross_constants::d2 = -0.1043000000000000;
const double Ross_constants::d3 =  0.1035000000000000;
const double Ross_constants::d4 = -0.3620000000000023e-1;

const double Ross_constants::w21 =  0.1544000000000000e1;
const double Ross_constants::w31 =  0.9466785280815826;
const double Ross_constants::w32 =  0.2557011698983284;
const double Ross_constants::w41 =  0.3314825187068521e1;
const double Ross_constants::w42 =  0.2896124015972201e1;
const double Ross_constants::w43 =  0.9986419139977817;
const double Ross_constants::w51 =  0.1221224509226641e1;
const double Ross_constants::w52 =  0.6019134481288629e1;
const double Ross_constants::w53 =  0.1253708332932087e2;
const double Ross_constants::w54 = -0.6878860361058950;

const double Ross_constants::z21 = -0.5668800000000000e1;
const double Ross_constants::z31 = -0.2430093356833875e1;
const double Ross_constants::z32 = -0.2063599157091915;
const double Ross_constants::z41 = -0.1073529058151375;
const double Ross_constants::z42 = -0.9594562251023355e1;
const double Ross_constants::z43 = -0.2047028614809616e2;
const double Ross_constants::z51 =  0.7496443313967647e1;
const double Ross_constants::z52 = -0.1024680431464352e2;
const double Ross_constants::z53 = -0.3399990352819905e2;
const double Ross_constants::z54 =  0.1170890893206160e2;
const double Ross_constants::z61 =  0.8083246795921522e1;
const double Ross_constants::z62 = -0.7981132988064893e1;
const double Ross_constants::z63 = -0.3152159432874371e2;
const double Ross_constants::z64 =  0.1631930543123136e2;
const double Ross_constants::z65 = -0.6058818238834054e1;

DISOL_NS_END
