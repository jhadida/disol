
//==================================================
// @title        ross.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <cmath>

// Base on:
// http://www.unige.ch/~hairer/prog/stiff/rodas.f



        /********************     **********     ********************/
        /********************     **********     ********************/



DISOL_NS_START

struct Ross_constants
{
    static const double
        gam,
        t2, t3, t4,
        d1, d2, d3, d4,
        w21,
        w31, w32,
        w41, w42, w43,
        w51, w52, w53, w54,
        z21,
        z31, z32,
        z41, z42, z43,
        z51, z52, z53, z54,
        z61, z62, z63, z64, z65;
};

// ------------------------------------------------------------------------

template <class S>
struct stepper_Ross
    : public stepper<S,4,true,true>, private Ross_constants
{
    using parent         = stepper<S,4,true,true>;
    using system_type    = S;
    using time_type      = typename parent::time_type;
    using value_type     = typename parent::value_type;
    using property_type  = typename parent::property_type;
    using handle_type    = typename parent::handle_type;

    // ----------  =====  ----------

    inline double smallest_substep() const { return t3; }

    stepper_Ross() { clear(); }

    void clear()
    {
        stepid = 0;
        y.clear();
        k1.clear();
        k2.clear();
        k3.clear();
        k4.clear();
        k5.clear();
        k6.clear();
        kdata.clear();
    }

    bool init( handle_type& h, property_type& prop )
        { return parent::init(h,prop) && resize(h.size()); }

    double step( handle_type& h, property_type& prop )
    {
        DRAYN_ASSERT_RV( h.valid(), 0.0, "Invalid handle.");
        const uidx_t nd = h.cur.size();
        

        // ----------  =====  ----------
        // Part 1: Evaluate K-points

        // Prepare k1 through k6 arrays
        auto& sys = *h.sys;
        auto dt  = h.cur.dt;
        auto x  = h.next.x;
        auto dx = h.next.dxdt;

        // compute only once per step, regardless of number of trials
        if ( stepid != h.next.index )
        {
            sys.derivative( h.cur.t, h.cur.x, h.cur.dxdt );
            sys.jacobian( h.cur.t, h.cur.x, h.cur.dfdx, h.cur.dfdt );

            // copy Jacobian matrix
            for ( uidx_t i=0; i<nd; i++ )
            for ( uidx_t j=0; j<nd; j++ )
                LU.lu(i,j) = -h.cur.dfdx(j,i); // algorithm assumes transpose matrix

            stepid = h.next.index;
        }

        // LU decomposition
        for ( uidx_t i=0; i<nd; i++ )
            LU.lu(i,i) = 1.0/(gam*dt) - h.cur.dfdx(i,i);
        LU.decompose();

        // K1
        algebra::iter_apply_3( x, h.cur.dxdt, h.cur.dfdt,
            algebra::weighted_sum_2<double>( 1.0, dt*d1 ) );
        LU.solve(x,k1);

        algebra::iter_apply_3( x, h.cur.x, k1,
            algebra::weighted_sum_2<double>( 1.0, w21 ) );
        sys.derivative( h.cur.t + dt*t2, x, dx );

        // K2
        algebra::iter_apply_4( x, dx, h.cur.dfdt, k1,
            algebra::weighted_sum_3<double>( 1.0, dt*d2, z21/dt ) );
        LU.solve(x,k2);

        algebra::iter_apply_4( x, h.cur.x, k1, k2,
            algebra::weighted_sum_3<double>( 1.0, w31, w32 ) );
        sys.derivative( h.cur.t + dt*t3, x, dx );

        // K3
        algebra::iter_apply_5( x, dx, h.cur.dfdt, k1, k2,
            algebra::weighted_sum_4<double>( 1.0, dt*d3, z31/dt, z32/dt ) );
        LU.solve(x,k3);

        algebra::iter_apply_5( x, h.cur.x, k1, k2, k3,
            algebra::weighted_sum_4<double>( 1.0, w41, w42, w43 ) );
        sys.derivative( h.cur.t + dt*t4, x, dx );

        // K4
        algebra::iter_apply_6( x, dx, h.cur.dfdt, k1, k2, k3,
            algebra::weighted_sum_5<double>( 1.0, dt*d4, z41/dt, z42/dt, z43/dt ) );
        LU.solve(x,k4);

        algebra::iter_apply_6( x, h.cur.x, k1, k2, k3, k4,
            algebra::weighted_sum_5<double>( 1.0, w51, w52, w53, w54 ) );
        sys.derivative( h.cur.t + dt, x, dx );

        // K5
        algebra::iter_apply_6( k6, dx, k1, k2, k3, k4,
            algebra::weighted_sum_5<double>( 1.0, z51, z52, z53, z54, 1.0/dt ) );
        LU.solve(k6,k5);

        sys.derivative( h.cur.t + dt, x, dx );

        // Compute the solution and the error
        algebra::iter_apply_7( k6, dx, k1, k2, k3, k4, k5,
            algebra::weighted_sum_6<double>( 1.0, z61, z62, z63, z64, z65, 1.0/dt ) );

        LU.solve(k6,y);


        // ----------  =====  ----------
        // Part 2: Evaluate error

        double error, scale, delta;
        error = 0.0;
        for ( uidx_t i = 0; i < nd; ++i )
        {
            x[i] += k5[i] + y[i];
            scale = std::max( dr::op_abs(h.cur.x[i]), dr::op_abs(h.next.x[i]) );
            // scale = std::max( prop.error.abs_tol, prop.error.rel_tol * scale );
            scale = prop.error.abs_tol + prop.error.rel_tol * scale;

            delta = y[i]/scale;
            error += delta*delta;
        }

        return sqrt( error / nd );
    }

private:

    bool resize( uidx_t n )
    {
        if ( kdata.size() != 7*n )
        {
            kdata.resize(7*n);
            LU.resize(n);

            k1.assign( &kdata[0*n], n );
            k2.assign( &kdata[1*n], n );
            k3.assign( &kdata[2*n], n );
            k4.assign( &kdata[3*n], n );
            k5.assign( &kdata[4*n], n );
            k6.assign( &kdata[5*n], n );
            y .assign( &kdata[6*n], n );
        }
        return true;
    }

    uidx_t stepid;
    svec<value_type>  kdata;
    pvec<value_type>  k1, k2, k3, k4, k5, k6, y;
    LUDecomposition<value_type> LU;
};

DISOL_NS_END
