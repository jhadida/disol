
//==================================================
// @title        srk2.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



DISOL_NS_START

//-------------------------------------------------------------
// Stochastic Runge-Kutta 2
// see: https://www.wikiwand.com/en/Runge%E2%80%93Kutta_method_(SDE)#/Variation_of_the_Improved_Euler_is_flexible
//-------------------------------------------------------------

template <class S>
struct stepper_SRK2: public stepper<S,2>
{
    using parent         = stepper<S,2>;
    using system_type    = S;
    using time_type      = typename parent::time_type;
    using value_type     = typename parent::value_type;
    using property_type  = typename parent::property_type;
    using handle_type    = typename parent::handle_type;

    using normal_dist    = std::normal_distribution<double>;
    using normal_gen     = dr::generator_type<normal_dist>;

    using bernoulli_dist = std::bernoulli_distribution;
    using bernoulli_gen  = dr::generator_type<bernoulli_dist>;

    static_assert( S::is_stochastic::value, "System should be stochastic." );

    // ----------  =====  ----------

    stepper_SRK2() { 
        m_normal = dr::normal_gen( 0.0, 1.0 );
        m_bernoulli = dr::bernoulli_gen( 0.5 );
        clear();
    }

    inline bool is_wiener() const { return m_wiener; }
    inline bool is_normal() const { return !m_wiener; }

    inline void make_wiener() { m_wiener = true; }
    inline void make_normal() { m_wiener = false; }

    inline void no_sqrt_dt() { make_normal(); }

    void clear() 
    { 
        k1.clear();
        k2.clear();
        kdata.clear(); 
        m_wiener = true;
    }

    bool init( handle_type& h, property_type& prop )
        { return parent::init(h,prop) && resize(h.size()); }

    double step( handle_type& h, property_type& prop )
    {
        DRAYN_DASSERT_RV( h.valid(), 0.0, "Invalid handle.");

        auto& sys = *h.sys;

        // generate random numbers
        const double w = m_normal();
        const double s = 1.0 - 2*m_bernoulli();
        const double fac = m_wiener ? sqrt(h.cur.dt) : 1.0;

        // compute k1
        sys.drift( h.cur.t, h.cur.x, h.cur.dxdt );
        sys.volatility( h.cur.t, h.cur.x, h.next.dxdt );

        algebra::iter_apply_3( k1, h.cur.dxdt, h.next.dxdt,
            algebra::weighted_sum_2<double>( h.cur.dt, fac*(w-s) ) );

        algebra::iter_apply_3( h.next.x, h.cur.x, k1,
            algebra::weighted_sum_2<double>( 1.0, 1.0 ) );

        // compute k2
        h.next.t = h.cur.t + h.cur.dt;
        sys.drift( h.next.t, h.next.x, h.cur.dxdt );
        sys.volatility( h.next.t, h.next.x, h.next.dxdt );

        algebra::iter_apply_3( k2, h.cur.dxdt, h.next.dxdt,
            algebra::weighted_sum_2<double>( h.cur.dt, fac*(w+s) ) );

        // final estimate
        algebra::iter_apply_4( h.next.x, h.cur.x, k1, k2,
            algebra::weighted_sum_3<double>( 1.0, 0.5, 0.5 ) );

        return 0.0;
    }

private:

    bool resize( uidx_t n )
    {
        if ( kdata.size() != 2*n ) 
        {
            kdata.resize(2*n);
            k1.assign( &kdata[0*n], n );
            k2.assign( &kdata[1*n], n );
        }
        return true;
    }

    svec<value_type> kdata;
    pvec<value_type> k1, k2;

    bool m_wiener;
    normal_gen m_normal;
    bernoulli_gen m_bernoulli;
};

DISOL_NS_END
