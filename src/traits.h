
//==================================================
// @title        traits.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <type_traits>



        /********************     **********     ********************/
        /********************     **********     ********************/



DISOL_NS_START

// alias for Drayn namespace
namespace dr = drayn;

// source integer types from Drayn
using namespace dr::integer;

// custom typenames for vector/matrix/timeseries
template <class T> using svec = dr::svec<T>;
template <class T> using pvec = dr::pvec<T>;

template <class T> using smat = dr::smat<T>;
template <class T> using pmat = dr::pmat<T>;

template <class V, class T=double> using ts_shr = dr::ts_shr<V,T>;
template <class V, class T=double> using ts_ptr = dr::ts_ptr<V,T>;

template <class V, class T=double> using tc_shr = dr::tc_shr<V,T>;
template <class V, class T=double> using tc_ptr = dr::tc_ptr<V,T>;

// ------------------------------------------------------------------------

template <class V, class T>
struct traits
{
    using time_type   = T;
    using value_type  = V;
    using pool_type   = dr::timepool<V,T>;

    using state_type  = typename pool_type::state_type;
    using deriv_type  = svec<V>;
    using array_type  = pvec<V>;
    using jacob_type  = smat<V>;
};

#define DISOL_TRAITS(V,T)                             \
typedef typename traits<V,T>::pool_type    pool_type; \
typedef typename traits<V,T>::time_type    time_type; \
typedef typename traits<V,T>::value_type  value_type; \
typedef typename traits<V,T>::state_type  state_type; \
typedef typename traits<V,T>::deriv_type  deriv_type; \
typedef typename traits<V,T>::array_type  array_type; \
typedef typename traits<V,T>::jacob_type  jacob_type;

DISOL_NS_END
