#ifndef DISOL_UTILS_H_INCLUDED
#define DISOL_UTILS_H_INCLUDED

//==================================================
// @title        include.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include "metric.h"
#include "interrupt.h"
#include "iter_apply.h"
#include "weighted_sum.h"
#include "lu.h"
#include "jacobian.h"

#endif
