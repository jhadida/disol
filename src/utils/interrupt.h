
//==================================================
// @title        interrupt.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#if DISOL_INTERRUPT_N > 0
    #ifdef __cplusplus
        extern "C" bool utIsInterruptPending();
    #else
        extern bool utIsInterruptPending();
    #endif
#endif

DISOL_NS_START
#if DISOL_INTERRUPT_N > 0

    inline bool interrup_check( unsigned every_N_call = 1 ) {
        static unsigned counter = 0;
        counter = (counter+1) % (every_N_call > 1? every_N_call : 1);
        return (counter == 0) && utIsInterruptPending();
    }

    inline void interrupt_throw( const char *msg = "Interruption detected.", unsigned every_N_call = 1 ) 
        { if ( interrup_check(every_N_call) ) throw std::runtime_error(msg); }

#else

    // shallow interface
    inline bool interrup_check( unsigned n=1 ) { return false; }
    inline void interrupt_throw( const char* m="null", unsigned n=1 ) {}

#endif
DISOL_NS_END
