
//==================================================
// @title        jacobian.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <vector>



        /********************     **********     ********************/
        /********************     **********     ********************/



DISOL_NS_START

enum class approx
    : char { Forward=1, Central=2 };

// ----------  =====  ----------

// evaluate Jacobian numerically
template <class V, class T = double>
class NumericJacobian
{
public:

    DISOL_TRAITS(V,T)

    value_type step;
    approx method;
    
    // ----------  =====  ----------
    
    NumericJacobian() 
        : step(1e-6), method(approx::Forward) {}

    void resize( uidx_t nd );
    inline uidx_t ndims() const { return m_before.size(); }

    template <class S>
    void operator() ( const S& sys, time_type t, const state_type& x, const jacob_type& J )
    {
        const uidx_t ns = sys.ndims();
        if ( ndims() != ns )
        {
            DRAYN_INFO( "Dimension mismatch with input system, resizing now." )
            resize( ns );
        }
        
        DRAYN_ASSERT_ERR( step >= dr::c_num<value_type>::eps, "Step is too small." )
        DRAYN_ASSERT_R( x.size() == ns, "Dimension mismatch with state size." )
        DRAYN_ASSERT_R( J.chksize(ns,ns), "Dimension mismatch with Jacobian." )

        switch ( method )
        {
            case approx::Forward:
                _forward_approx(sys,t,x,J); break;

            case approx::Central:
                _central_approx(sys,t,x,J); break;

            default:
                throw std::runtime_error("Unknown method for numerical jacobian.");
        }
    }

private:

    /**
     * Implementation using central approximation.
     * Cost: 2*ndims calls to the derivative function.
     */
    template <class S>
    void _central_approx( const S& sys, time_type t, const state_type& x, const jacob_type& J );

    /**
     * Implementation using forward approximation.
     * Cost: ndims calls to the derivative function.
     */
    template <class S>
    void _forward_approx( const S& sys, time_type t, const state_type& x, const jacob_type& J );

    std::vector<value_type> m_before, m_after;
};



        /********************     **********     ********************/
        /********************     **********     ********************/



template <class V, class T>
void NumericJacobian<V,T>::resize( uidx_t nd )
{
    DRAYN_ASSERT_R( nd > 1, "There should be more than 1 state." );
    m_before.resize(nd);
    m_after.resize(nd);
}

// ------------------------------------------------------------------------

template <class V, class T>
template <class S>
void NumericJacobian<V,T>::_central_approx( const S& sys, time_type t, const state_type& x, const jacob_type& J )
{
    const uidx_t ns = ndims();
    value_type current;

    // Iterate on the rows of the Jacobian
    for ( uidx_t r = 0; r < ns; ++r )
    {
        current = x[r];

        // Evaluate a little before
        x[r] = current - step;
        sys.derivative( t, x, m_before );

        // And after
        x[r] = current + step;
        sys.derivative( t, x, m_after );

        // Restore current value
        x[r] = current;

        // Compute the derivative with central approximation
        for ( uidx_t c = 0; c < ns; ++c )
            J(r,c) = ( m_after[c] - m_before[c] ) / ( 2*step );
    }
}

// ------------------------------------------------------------------------

template <class V, class T>
template <class S>
void NumericJacobian<V,T>::_forward_approx( const S& sys, time_type t, const state_type& x, const jacob_type& J )
{
    const uidx_t ns = ndims();
    value_type current;

    // Use the "before" vector to store the current derivative
    sys.derivative( t, x, m_before );

    // Iterate on the rows of the Jacobian
    for ( uidx_t r = 0; r < ns; ++r )
    {
        current = x[r];

        // And after
        x[r] = current + step;
        sys.derivative( t, x, m_after );

        // Restore current value
        x[r] = current;

        // Compute the derivative with central approximation
        for ( uidx_t c = 0; c < ns; ++c )
            J(r,c) = ( m_after[c] - m_before[c] ) / step;
    }
}

DISOL_NS_END
