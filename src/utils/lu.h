
//==================================================
// @title        lu.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <cmath>
#include <vector>
#include <type_traits>



        /********************     **********     ********************/
        /********************     **********     ********************/



DISOL_NS_START

template <class T>
struct LUDecomposition
{
    static_assert( std::is_floating_point<T>::value, "Floating-point value-type required." );

    using Integer = int64_t;
    using Value   = T;
    using Matrix  = smat<Value>;
    using Indices = svec<Integer>;
    using Vector  = svec<Value>;
    using Pointer = pvec<Value>;

    Integer n;
    Matrix lu;
    Indices idx;

    LUDecomposition(): n(0) {}

    explicit LUDecomposition( Integer size )
        { resize(size); }
    explicit LUDecomposition( const Matrix& a )
        { assign(a); decompose(); }

    void resize( Integer size );
    void assign( const Matrix& a );

    void decompose();
    void solve( const Pointer& b, const Pointer& x ) const;

private:
    mutable Vector tvec; // temporary vec
};

// ------------------------------------------------------------------------

template <class T>
void LUDecomposition<T>::resize( Integer size )
{
    n = size;
    lu = dr::make_mat<T>(n,n);
    idx.resize(n);
    tvec.resize(n);
}

template <class T>
void LUDecomposition<T>::assign( const Matrix& a )
{
    n = a.nrows();
    lu = a;
    idx.resize(n);
    tvec.resize(n);
}

template <class T>
void LUDecomposition<T>::decompose()
{
    const Value EPS = dr::c_num<Value>::eps;
    Integer i, j, k, imax;
    Value big, tmp;

    for (i=0;i<n;i++)
    {
        // find absolute largest element in row
        big=0;
        for (j=0;j<n;j++)
            if ( (tmp=dr::op_abs(lu(i,j))) > big )
                big=tmp;

        // store its inverse
        DRAYN_REJECT_ERR( big < EPS, "Singular matrix." )
        tvec[i]=1/big;
    }

    for (k=0;k<n;k++)
    {
        big=0;
        for (i=k;i<n;i++)
        {
            tmp=tvec[i]*dr::op_abs(lu(i,k));
            if (tmp > big)
            {
                big=tmp;
                imax=i;
            }
        }

        if (k != imax)
        {
            for (j=0;j<n;j++)
            {
                tmp=lu(imax,j);
                lu(imax,j)=lu(k,j);
                lu(k,j)=tmp;
            }
            tvec[imax]=tvec[k];
        }

        idx[k]=imax;
        if ( lu(k,k) >= 0 )
            lu(k,k) = std::max( lu(k,k), EPS );
        else
            lu(k,k) = std::min( lu(k,k), -EPS );

        for (i=k+1;i<n;i++)
        {
            tmp=(lu(i,k) /= lu(k,k));
            for (j=k+1;j<n;j++)
                lu(i,j) -= tmp*lu(k,j);
        }
    }
}

template <class T>
void LUDecomposition<T>::solve( const Pointer& b, const Pointer& x ) const
{
    Integer i, j, ip;
    Integer k = 0;
    Value sum;

    DRAYN_ASSERT_ERR( b.size() != n || x.size() != n, "Bad input size." )

    for (i=0;i<n;i++)
        x[i] = b[i];

    for (i=0;i<n;i++)
    {
        ip=idx[i];
        sum=x[ip];
        x[ip]=x[i];

        if (k != 0)
            for (j=k-1;j<i;j++)
                sum -= lu(i,j)*x[j];
        else if (sum != 0)
            k=i+1;

        x[i]=sum;
    }

    for (i=n-1;i>=0;i--)
    {
        sum=x[i];
        for (j=i+1;j<n;j++)
            sum -= lu(i,j)*x[j];

        x[i]=sum/lu(i,i);
    }
}

DISOL_NS_END
