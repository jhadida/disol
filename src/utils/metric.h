
//==================================================
// @title        metric.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <cmath>



        /********************     **********     ********************/
        /********************     **********     ********************/



DISOL_NS_START

struct no_metric {};

struct metric_L2
{
    template <class T>
    double operator() ( const pvec<T>& x ) const {
        double m = 0.0;
        for ( uidx_t i=0; i < x.size(); i++ )
            m += x[i]*x[i];
        return sqrt(m);
    }
};

using default_metric = metric_L2;

DISOL_NS_END
