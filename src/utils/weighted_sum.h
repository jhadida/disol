
//==================================================
// @title        weighted_sum.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



DISOL_NS_START

namespace algebra {

    template <class W = double>
    struct weighted_sum_1
    {
        const W m_w1;

        weighted_sum_1( W w1 )
            : m_w1(w1)
            {}

        template <class Tout, class T1>
        inline void operator() ( Tout& out, T1 t1 ) const
        {
            out = static_cast<Tout>( m_w1*t1 );
        }
    };

    template <class W = double>
    struct weighted_sum_2
    {
        const W m_w1, m_w2;

        weighted_sum_2( W w1, W w2 )
            : m_w1(w1), m_w2(w2)
            {}

        template <class Tout, class T1, class T2>
        inline void operator() ( Tout& out, T1 t1, T2 t2 ) const
        {
            out = static_cast<Tout>( m_w1*t1 + m_w2*t2 );
        }
    };

    template <class W = double>
    struct weighted_sum_3
    {
        const W m_h;
        const W m_w1, m_w2, m_w3;

        weighted_sum_3( W w1, W w2, W w3, W h=1.0 )
            : m_w1(w1), m_w2(w2), m_w3(w3), m_h(h)
            {}

        template <class Tout, class T1, class T2, class T3>
        inline void operator() ( Tout& out, T1 t1, T2 t2, T3 t3 ) const
        {
            out = static_cast<Tout>( m_w1*t1 + m_h*(m_w2*t2 + m_w3*t3) );
        }
    };

    template <class W = double>
    struct weighted_sum_4
    {
        const W m_h;
        const W m_w1, m_w2, m_w3, m_w4;

        weighted_sum_4( W w1, W w2, W w3, W w4, W h=1.0 )
            : m_w1(w1), m_w2(w2), m_w3(w3), m_w4(w4), m_h(h)
            {}

        template <class Tout, class T1, class T2, class T3, class T4>
        inline void operator() ( Tout& out, T1 t1, T2 t2, T3 t3, T4 t4 ) const
        {
            out = static_cast<Tout>( m_w1*t1 + m_h*(m_w2*t2 + m_w3*t3 + m_w4*t4) );
        }
    };

    template <class W = double>
    struct weighted_sum_5
    {
        const W m_h;
        const W m_w1, m_w2, m_w3, m_w4, m_w5;

        weighted_sum_5( W w1, W w2, W w3, W w4, W w5, W h=1.0 )
            : m_w1(w1), m_w2(w2), m_w3(w3), m_w4(w4), m_w5(w5), m_h(h)
            {}

        template <class Tout, class T1, class T2, class T3, class T4, class T5>
        inline void operator() ( Tout& out, T1 t1, T2 t2, T3 t3, T4 t4, T5 t5 ) const
        {
            out = static_cast<Tout>( m_w1*t1 + m_h*(m_w2*t2 + m_w3*t3 + m_w4*t4 + m_w5*t5) );
        }
    };

    template <class W = double>
    struct weighted_sum_6
    {
        const W m_h;
        const W m_w1, m_w2, m_w3, m_w4, m_w5, m_w6;

        weighted_sum_6( W w1, W w2, W w3, W w4, W w5, W w6, W h=1.0 )
            : m_w1(w1), m_w2(w2), m_w3(w3), m_w4(w4), m_w5(w5), m_w6(w6), m_h(h)
            {}

        template <class Tout, class T1, class T2, class T3, class T4, class T5, class T6>
        inline void operator() ( Tout& out, T1 t1, T2 t2, T3 t3, T4 t4, T5 t5, T6 t6 ) const
        {
            out = static_cast<Tout>( m_w1*t1 + m_h*(m_w2*t2 + m_w3*t3 + m_w4*t4 + m_w5*t5 + m_w6*t6) );
        }
    };

    template <class W = double>
    struct weighted_sum_7
    {
        const W m_h;
        const W m_w1, m_w2, m_w3, m_w4, m_w5, m_w6, m_w7;

        weighted_sum_7( W w1, W w2, W w3, W w4, W w5, W w6, W w7, W h=1.0 )
            : m_w1(w1), m_w2(w2), m_w3(w3), m_w4(w4), m_w5(w5), m_w6(w6), m_w7(w7), m_h(h)
            {}

        template <class Tout, class T1, class T2, class T3, class T4, class T5, class T6, class T7>
        inline void operator() ( Tout& out, T1 t1, T2 t2, T3 t3, T4 t4, T5 t5, T6 t6, T7 t7 ) const
        {
            out = static_cast<Tout>( m_w1*t1 + m_h*(m_w2*t2 + m_w3*t3 + m_w4*t4 + m_w5*t5 + m_w6*t6 + m_w7*t7) );
        }
    };

    template <class W = double>
    struct weighted_sum_8
    {
        const W m_h;
        const W m_w1, m_w2, m_w3, m_w4, m_w5, m_w6, m_w7, m_w8;

        weighted_sum_8( W w1, W w2, W w3, W w4, W w5, W w6, W w7, W w8, W h=1.0 )
            : m_w1(w1), m_w2(w2), m_w3(w3), m_w4(w4), m_w5(w5), m_w6(w6), m_w7(w7), m_w8(w8), m_h(h)
            {}

        template <class Tout, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8>
        inline void operator() ( Tout& out, T1 t1, T2 t2, T3 t3, T4 t4, T5 t5, T6 t6, T7 t7, T8 t8 ) const
        {
            out = static_cast<Tout>( m_w1*t1 + m_h*(m_w2*t2 + m_w3*t3 + m_w4*t4 + m_w5*t5 + m_w6*t6 + m_w7*t7 + m_w8*t8) );
        }
    };

    template <class W = double>
    struct weighted_sum_9
    {
        const W m_h;
        const W m_w1, m_w2, m_w3, m_w4, m_w5, m_w6, m_w7, m_w8, m_w9;

        weighted_sum_9( W w1, W w2, W w3, W w4, W w5, W w6, W w7, W w8, W w9, W h=1.0 )
            : m_w1(w1), m_w2(w2), m_w3(w3), m_w4(w4), m_w5(w5), m_w6(w6), m_w7(w7), m_w8(w8), m_w9(w9), m_h(h)
            {}

        template <class Tout, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, class T9>
        inline void operator() ( Tout& out, T1 t1, T2 t2, T3 t3, T4 t4, T5 t5, T6 t6, T7 t7, T8 t8, T9 t9 ) const
        {
            out = static_cast<Tout>( m_w1*t1 + m_h*(m_w2*t2 + m_w3*t3 + m_w4*t4 + m_w5*t5 + m_w6*t6 + m_w7*t7 + m_w8*t8 + m_w9*t9) );
        }
    };

    template <class W = double>
    struct weighted_sum_10
    {
        const W m_h;
        const W m_w1, m_w2, m_w3, m_w4, m_w5, m_w6, m_w7, m_w8, m_w9, m_w10;

        weighted_sum_10( W w1, W w2, W w3, W w4, W w5, W w6, W w7, W w8, W w9, W w10, W h=1.0 )
            : m_w1(w1), m_w2(w2), m_w3(w3), m_w4(w4), m_w5(w5), m_w6(w6), m_w7(w7), m_w8(w8), m_w9(w9), m_w10(w10), m_h(h)
            {}

        template <class Tout, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, class T9, class T10>
        inline void operator() ( Tout& out, T1 t1, T2 t2, T3 t3, T4 t4, T5 t5, T6 t6, T7 t7, T8 t8, T9 t9, T10 t10 ) const
        {
            out = static_cast<Tout>( m_w1*t1 + m_h*(m_w2*t2 + m_w3*t3 + m_w4*t4 + m_w5*t5 + m_w6*t6 + m_w7*t7 + m_w8*t8 + m_w9*t9 + m_w10*t10) );
        }
    };

}

DISOL_NS_END
